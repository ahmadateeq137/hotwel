<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/countdowntime/flipclock.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!-- Fonts -->

    <!-- Styles -->
    <style>

    </style>
</head>

<body>
    <div style="width:100%; text-align:center">
        <div class="text-center align-items-center"><img src="https://i.ibb.co/8K7hyf0/logo.png" alt="logo" height="80"></div><br />
        <p>Hello <i>{{ $prescription->receiver }}</i>,HotWell Here!</p>
        <p>Your Account Password is Successfully Reset!</p><br />
        Thank You,
        <br />
        <i>{{ $prescription->sender }}</i>
        <br />
    </div>
</body>

</html>