<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeoSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('meta_title')->nullable()->unique();
            $table->longText('meta_description')->nullable();
            $table->string('static_page_id')->nullable();
            $table->string('custom_page_id')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('featured_image')->nullable();
            $table->string('facebook_link')->nullable();
            $table->string('facebook_title')->nullable();
            $table->longText('facebook_description')->nullable();
            $table->string('twitter_link')->nullable();
            $table->string('twitter_title')->nullable();
            $table->longText('twitter_description')->nullable();
              $table->timestamp('created_at')->useCurrent();
    $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seo_settings');
    }
}
