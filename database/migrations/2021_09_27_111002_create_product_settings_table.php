<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_settings', function (Blueprint $table) {
            $table->increments('id')->nullable();
            $table->time('start_time');
            $table->time('end_time');
            $table->integer('temperature');
            $table->boolean('gass')->default(0);
            $table->boolean('electricity')->default(0);
            $table->boolean('auto')->default(0);
            $table->boolean('status')->default(0);
            $table->integer('product_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_settings');
    }
}
