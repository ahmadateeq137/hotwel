<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsSlidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_slides', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->unique();
            $table->bigInteger('cms_slider_id');
            $table->string('image')->nullable();
            $table->string('rating')->nullable();
            $table->longText('description')->nullable();
            $table->string('amount')->nullable();
            $table->date('date_from')->nullable();
            $table->date('date_to')->nullable();
            $table->enum('status', ['active', 'inactive',]);
            $table->enum('featured', ['0', '1',]);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_slides');
    }
}
