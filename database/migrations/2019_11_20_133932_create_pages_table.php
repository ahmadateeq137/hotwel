<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('seo_setting_id')->nullable();
            $table->string('title')->unique();
            $table->string('type');
            $table->string('target');
            $table->string('external_link')->nullable();
            $table->string('slug')->nullable()->unique();
            $table->string('description1_heading')->nullable();
            $table->longText('description1')->nullable();
            $table->string('description1_image')->nullable();
            $table->string('description2_heading')->nullable();
            $table->longText('description2')->nullable();
            $table->string('description2_image')->nullable();
            $table->string('featured_image')->nullable();
            $table->integer('order_by');
            $table->string('menu');
            $table->enum('status', ['active', 'inactive',]);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
