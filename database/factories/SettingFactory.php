<?php

namespace Database\Factories;

use App\Models\Setting;
use App\Models\Timer;
use Illuminate\Database\Eloquent\Factories\Factory;

class SettingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Setting::class;

    protected $days = ['SUN','MON','TUE','WED','THU','FRI','SAT'];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
                "key" => '2',
                "day" => 'SUN',
                "description"=>"testing",
                "value" => 'test',
                "status" => 'inactive',
                "timer_id" => ''
        ];
    }
}
