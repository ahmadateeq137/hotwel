<?php

namespace Database\Seeders;

use App\Models\CMS\Page;
use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::insert([
            [
                "seo_setting_id" => '1',
                "title" => 'about',
                "slug" => 'about',
                "type" => 'page',
                "target" => '',
                "external_link" => 'https://www.facebook.com/',
                "description1" => 'We use your Personal Information in ways that are aligned with the purposes for which it was collected or authorized by you, including the following purposes
                To investigate possible fraud or other violations of our Terms of Use or this Privacy Policy and/or attempts to harm our Users.',
                "description1_heading" => 'About Us',
                "description1_image" => '',
                "description2" => '',
                "description2_heading" => '',
                "description2_image" => '',
                "menu" => 'page',
                "featured_image" => '',
                "order_by" => '1',
                "status"=>"active",
            ],
            
        ]);
    }
}
