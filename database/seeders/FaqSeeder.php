<?php

namespace Database\Seeders;

use App\Models\CMS\Faq;
use Illuminate\Database\Seeder;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Faq::insert([
            [
                "order_by" => '1',
                "question" => 'How can I refill my medication order?',
                "answer" => 'As long as we have a valid prescription on file we are able to process your refill order.  We will remind you to update your prescription close to the expiry of the one on file.',
                "status"=>"active",
            ],
            [
                "order_by" => '1',
                "question" => 'Can I use my insurance plan for payment?',
                "answer" => 'patients can certainly use their insurance plan to pay for their      medications.',
                "status"=>"active",
            ],[
                "order_by" => '1',
                "question" => 'How does it take to process an order?',
                "answer" => 'We endeavor to process each order we receive on any given day on that same day.',
                "status"=>"active",
            ],[
                "order_by" => '1',
                "question" => 'Can I cancel my order after it has been submitted?',
                "answer" => 'Anything is possible! We do try to process all orders within 24 hours of being received. If your order has not yet been processed and forwarded to the pharmacy for dispensing, you can cancel your order. Just give us a call at our toll-free number, and we will take care of you.',
                "status"=>"active",
            ],[
                "order_by" => '1',
                "question" => 'Do I need a Prescription to order from you?',
                "answer" => 'Yes, we are no different from your local pharmacy. Prescription items are clearly marked as requiring a prescription. In order to dispense these items, we need a valid prescription from a licensed physician',
                "status"=>"active",
            ],
           
        ]);
    }
}
