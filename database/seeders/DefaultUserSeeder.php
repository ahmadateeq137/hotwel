<?php

namespace Database\Seeders;

use App\Repositories\UserRepositoryInterface;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;
use App\Repositories\PermissionRepositoryInterface;

// use Illuminate\Support\Facades\Hash;

class DefaultUserSeeder extends Seeder
{
    /**      
     * @var UserRepositoryInterface
     */
    private $repository;


    /**      
     * @var PermissionRepositoryInterface
     */
    private $permissionRepository;

    /**      
     * @var Hash
     */
    private $hash;

    /**      
     * DefaultUserSeeder constructor.      
     *      
     * @param UserRepositoryInterface $repository      
     * @param Hash $hash
     * @return void
     */
    public function __construct(UserRepositoryInterface $repository, PermissionRepositoryInterface $permissionRepository)
    {
        $this->repository = $repository;
        $this->permissionRepository = $permissionRepository;
    }
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'first_name' => 'User',
            'last_name'=>'Hotwell',
            'password' => Hash::make(config('default_seeding_credentials.user_password')),
            "email" => config('default_seeding_credentials.user_email'),
            "phone_number"=>'+923232222222',
            "isVerified"=>'1'
        ];
        $item = $this->repository->create($data);

        $this->repository->assignRole($item, 'User');
        $this->permissionRepository->guard_name = 'user';
        $permissions = $this->permissionRepository->allNames();
        $this->repository->assignPermission($item, $permissions);
        
        $this->command->info('User email'. config('default_seeding_credentials.user_email').' and password'.config('default_seeding_credentials.user_password'));
    }
}
