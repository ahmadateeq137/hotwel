<?php

namespace Database\Seeders;

use App\Repositories\PermissionRepositoryInterface;
use App\Repositories\RoleRepositoryInterface;
use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
     /**      
     * @var RoleRepositoryInterface
     */
    private $roleRepository;

    /**      
     * @var PermissionRepositoryInterface
     */
    private $permissionRepository;

    public function __construct()
    {
        $guard_name = 'user';
        $this->roleRepository = resolve(RoleRepositoryInterface::class);
        $this->roleRepository->guard_name = $guard_name;
        $this->permissionRepository = resolve(PermissionRepositoryInterface::class);
        $this->permissionRepository->guard_name = $guard_name;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = $this->permissionRepository->allIds();
        $this->roleRepository->create([
            'name' => 'User',
            'permissions' => $permissions
        ]);
    }
}
