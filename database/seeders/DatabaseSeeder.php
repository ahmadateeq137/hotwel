<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserPermissionSeeder::class);
        $this->call(UserRoleSeeder::class);
        $this->call(DefaultUserSeeder::class);
        $this->call(PagesSeeder::class);
        $this->call(FaqSeeder::class);
        $this->call(CategorySeeder::class);
    }
}
