<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class UserPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions  =  [
            "View User",
            "Update User",
            "Delete User",

            'Create Contact',
            "View Contact",
            "Update Contact",
            "Delete Contact",

            'Create page',
            "View page",
            "Update page",
            "Delete page",
            
            'Create Category',
            "View Category",
            "Update Category",
            "Delete Category",
            
            'Create product',
            "View product",
            "Update product",
            "Delete product",

            'Create Timer',
            "View Timer",
            "Update Timer",
            "Delete Timer",

            'Create Faq',
            "View Faq",
            "Update Faq",
            "Delete Faq",
        ];
        foreach ($permissions as $permission) {
            Permission::create([
                'name' => $permission,
                'guard_name' => 'user',
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
