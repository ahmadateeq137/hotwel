<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    // public function __construct(User $user)
    // {
    //     return $user;
    // }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::latest('id')->first();
            Category::insert([
            [
                "name" => "geyser",
                "image"=>"https://ibb.co/LRS6q6W",
                "description"=>"Water Heater",
                "user_id" => $users->id
            ],
            [
                "name" => "Boiler",
                "image"=>"https://ibb.co/xq49FjS",
                "description"=>"Room Heater",
                "user_id" => $users->id
            ]
        ]);  
    }
}
