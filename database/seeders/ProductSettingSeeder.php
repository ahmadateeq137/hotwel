<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\ProductSetting;
use Illuminate\Database\Seeder;

class ProductSettingSeeder extends Seeder
{
    public function __construct(Product $product)
    {
        return $product;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::latest('id')->first();
        ProductSetting::insert([
            [
                // 'gass'=>'nullable',
                // 'electricity'=>'nullable',
                // 'gas_status'=>'nullable',
                // 'electricity_status'=>'nullable',
                // 'timer_status'=>'nullable',
                // 'auto'=>'nullable',
                'temperature'=>'45',
                // 'status'=>'nullable',
                'product_id'=> $products->id,
            ],
        ]);
    }
}
