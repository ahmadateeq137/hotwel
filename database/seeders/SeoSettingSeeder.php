<?php

namespace Database\Seeders;

use App\Models\CMS\SeoSetting;
use Illuminate\Database\Seeder;

class SeoSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SeoSetting::insert([
            [
                "meta_title" => '1',
                "meta_description" => 'page',
                "static_page_id" => 'new',
                "custom_page_id"=>"testing",
                "meta_keywords" => 'test',
                "featured_image" => '',
                "facebook_link" => 'active',
                "facebook_title"=>"testing",
                "facebook_description" => 'test',
                "twitter_link" => '',
                "twitter_title" => 'active',
                "twitter_description" => 'active',
            ],
        ]);
    }
}
