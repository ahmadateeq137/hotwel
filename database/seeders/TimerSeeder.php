<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\Timer;
use Illuminate\Database\Seeder;

class TimerSeeder extends Seeder
{
    public function __construct(Product $product)
    {
        return $product;
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::latest('id')->first();
        Timer::insert([
            [
                'name' => 'timer1',
                // 'monday'=>'nullable',
                // 'tuesday'=>'nullable',
                // 'wednesday'=>'nullable',
                // 'thursday'=>'nullable',
                // 'friday'=>'nullable',
                // 'saturday'=>'nullable',
                // 'sunday'=>'nullable',
                'start_time'=>'06:00',
                'end_time'=>'06:59',
                // 'gass'=>'nullable',
                // 'electricity'=>'nullable',
                // 'auto'=>'nullable',
                'temperature'=>'45',
                'status'=>0,
                // 'gas_status'=>0,
                // 'electricity_status'=>0,
                'product_id'=> $products->id,
            ],
            [
                'name' => 'timer2',
                // 'monday'=>'nullable',
                // 'tuesday'=>'nullable',
                // 'wednesday'=>'nullable',
                // 'thursday'=>'nullable',
                // 'friday'=>'nullable',
                // 'saturday'=>'nullable',
                // 'sunday'=>'nullable',
                'start_time'=>'07:00',
                'end_time'=>'07:59',
                // 'gass'=>'nullable',
                // 'electricity'=>'nullable',
                // 'auto'=>'nullable',
                'temperature'=>'45',
                'status'=>0,
                // 'gas_status'=>0,
                // 'electricity_status'=>0,
                'product_id'=> $products->id,
            ],
            [
                'name' => 'timer3',
                // 'monday'=>'nullable',
                // 'tuesday'=>'nullable',
                // 'wednesday'=>'nullable',
                // 'thursday'=>'nullable',
                // 'friday'=>'nullable',
                // 'saturday'=>'nullable',
                // 'sunday'=>'nullable',
                'start_time'=>'08:00',
                'end_time'=>'08:59',
                // 'gass'=>'nullable',
                // 'electricity'=>'nullable',
                // 'auto'=>'nullable',
                'temperature'=>'45',
                'status'=>0,
                // 'gas_status'=>0,
                // 'electricity_status'=>0,
                'product_id'=> $products->id,
            ],
            [
                'name' => 'timer4',
                // 'monday'=>'nullable',
                // 'tuesday'=>'nullable',
                // 'wednesday'=>'nullable',
                // 'thursday'=>'nullable',
                // 'friday'=>'nullable',
                // 'saturday'=>'nullable',
                // 'sunday'=>'nullable',
                'start_time'=>'09:00',
                'end_time'=>'09:59',
                // 'gass'=>'nullable',
                // 'electricity'=>'nullable',
                // 'auto'=>'nullable',
                'temperature'=>'45',
                'status'=>0,
                // 'gas_status'=>0,
                // 'electricity_status'=>0,
                'product_id'=> $products->id,
            ],
            [
                'name' => 'timer5',
                // 'monday'=>'nullable',
                // 'tuesday'=>'nullable',
                // 'wednesday'=>'nullable',
                // 'thursday'=>'nullable',
                // 'friday'=>'nullable',
                // 'saturday'=>'nullable',
                // 'sunday'=>'nullable',
                'start_time'=>'10:00',
                'end_time'=>'10:59',
                // 'gass'=>'nullable',
                // 'electricity'=>'nullable',
                // 'auto'=>'nullable',
                'temperature'=>'45',
                'status'=>0,
                // 'gas_status'=>0,
                // 'electricity_status'=>0,
                'product_id'=> $products->id,
            ],
            [
                'name' => 'timer6',
                // 'monday'=>'nullable',
                // 'tuesday'=>'nullable',
                // 'wednesday'=>'nullable',
                // 'thursday'=>'nullable',
                // 'friday'=>'nullable',
                // 'saturday'=>'nullable',
                // 'sunday'=>'nullable',
                'start_time'=>'11:00',
                'end_time'=>'11:59',
                // 'gass'=>'nullable',
                // 'electricity'=>'nullable',
                // 'auto'=>'nullable',
                'temperature'=>'45',
                'status'=>0,
                // 'gas_status'=>0,
                // 'electricity_status'=>0,
                'product_id'=> $products->id,
            ]
        ]);    
    }
}
