<?php

namespace App\Models;

use App\Models\Setting;
use Illuminate\Database\Eloquent\Model;

class Timer extends Model
{
    protected $fillable = ['name', 'start_time', 'end_time', 'gass', 'gas_status', 'electricity', 'electricity_status', 'electricity', 'auto', 'status', 'temperature', 'product_id','monday','tuesday','wednesday','thursday','friday','saturday','sunday'];


    public function products()
    {
        return $this->belongsTo(Product::class,'product_id');
    }
}