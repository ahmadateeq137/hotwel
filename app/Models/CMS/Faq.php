<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Faq extends Model
{
    use  Notifiable;
    protected $fillable = ['order_by' , 'question' , 'answer','status'];
    protected $table='faqs';
}
