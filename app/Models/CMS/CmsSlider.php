<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;


class CmsSlider extends Model
{
    

    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','description','cover_photo','status','featured',
    ];
   
    public function cmsSlide()
    {
        return $this->hasMany(CmsSlide::class);
    }
   
}
