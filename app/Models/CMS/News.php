<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = ['news_category_id','title', 'category_name','description', 'featured_image', 'image', 'status', 'featured','meta_title', 'meta_description', 'meta_keywords',];

    public function newsCategory()
    {
        return $this->belongsTo(NewsCategory::class);
    }
}

