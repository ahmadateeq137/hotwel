<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class SiteLogo extends Model
{
    protected $fillable = ['header_logo', 'footer_logo', 'favicon'];
}
