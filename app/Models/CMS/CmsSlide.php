<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;


class CmsSlide extends Model
{

    //
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','cms_slider_id','image','rating','description','amount','date_from','date_to','status','featured',
    ];
 
    public function cmsSlider()
    {
        return $this->belongsTo(CmsSlider::class);
    }
}
