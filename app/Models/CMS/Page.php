<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['seo_setting_id','title','slug','description1_heading','description1','description1_image','description2_heading','description2','description2_image', 'menu','order_by',
    'featured_image','status','type','target','external_link'];
    
    public function seoSetting()
    {
        return $this->belongsTo(SeoSetting::class);
    }

    public static function boot()
    {
        parent::boot();
        static::deleted(function ($obj) {
            $obj->seoSetting()->delete();
         
        });
    }
    
}
