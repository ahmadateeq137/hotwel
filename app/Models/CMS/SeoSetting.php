<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class SeoSetting extends Model
{
    protected $fillable = ['static_page_id','custom_page_id','meta_title','meta_description','meta_keywords','featured_image','facebook_link',
    'facebook_title','facebook_description','twitter_link',
    'twitter_title','twitter_description'];

    public function pages()
    {
        return $this->hasMany(Page::class);
    }
    
}
