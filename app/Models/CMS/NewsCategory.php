<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;


class NewsCategory extends Model
{

    //
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function news()
    {
        return $this->hasMany(News::class);
    }

}
