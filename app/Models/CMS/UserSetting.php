<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserSetting extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'user_settings';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','key','value','user_id',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

}
