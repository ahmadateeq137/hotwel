<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;
use App\Models\Role;
use Database\Seeders\CategorySeeder;
use Database\Seeders\TimerSeeder;

class User extends Authenticatable
{
    use HasFactory, Notifiable,HasApiTokens,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone_number',
        'verification_token',
        'isVerified',
        'mac_address',
        'password',
        'image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    /**
     * A model may have multiple roles.
     */
    public function roles(): MorphToMany
    {
        return $this->morphToMany(
            Role::class,
            'model',
            config('permission.table_names.model_has_roles'),
            config('permission.column_names.model_morph_key'),
            'role_id'
        );
    }

    public function getRoleClass()
    {
        return resolve(Role::class);
    }

    public function contacts(){
        return $this->hasMany(Contact::class);
    }

    public function settings()
    {
        return $this->belongsToMany(Setting::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class,'category_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class,);
    }

    public static function boot()
    {
        parent::boot();
        $cat = Timer::find('product_id');
        static::deleting(function($user) { 
            $user->products()->each(function($cat){ //edit after comment
                $cat->delete();
              });
       });

    }
}