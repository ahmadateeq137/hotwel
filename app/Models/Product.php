<?php

namespace App\Models;

use Database\Seeders\ProductSettingSeeder;
use Database\Seeders\TimerSeeder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = ['mac_address', 'auto_shift', 'user_id','category_id'];

    public function categories()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function timers()
    {
        return $this->hasMany(Timer::class,'product_id');
    }
    
    public function productSettings()
    {
        return $this->hasOne(ProductSetting::class,'product_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($obj) {
        $category = resolve(TimerSeeder::class, [
        'timer' => $obj
        ]);
        $category->run();
        });
        
        static::created(function ($obj) {
            $category = resolve(ProductSettingSeeder::class, [
            'productSetting' => $obj
            ]);
            $category->run();
        });

        static::deleting(function($obj) { 
            $obj->timers()->delete();
        });
        
        static::deleting(function($obj) { 
            $obj->productSettings()->delete();
        });
    }
}
