<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductSetting extends Model
{
    use HasFactory;
    protected $fillable = ['start_time', 'end_time', 'gass', 'gas_status', 'temperature_status', 'electricity', 'electricity_status', 'auto', 'status', 'temperature', 'product_id'];

    public function products()
    {
        return $this->belongsTo(Product::class,'product_id');
    }
}
