<?php
/**
 * Send success response to the request.
 *
 * @param  array  $data
 * @param  int  $code
 * @param  string  $message
 * @return \Illuminate\Http\JsonResponse
 */
function successResponse($data = [], $code = 200, $message = null)
{
    if (!$message) {
        $message = __('httpmessages.200');
    }
    if (gettype($data) == 'object' && ("App\Resources\BaseResource" == get_parent_class(get_class($data)) || "Illuminate\Http\Resources\Json\ResourceCollection" == get_parent_class(get_class($data)))) {
        $status = [
            'status' => [
                'success' => true,
                'code' => $code,
                'message' => $message
            ],
        ];
        return $data->additional($status);
    } else {
        return response()->json(
            [
                'status' => [
                    'success' => true,
                    'code' => $code,
                    'message' => $message,
                ],
                'data' => $data,
            ]
        );
    }
}
// function successResponse($data = [], $code = 200, $message = 'OK')
// {
// return response()->json(
//     [
//         'success' => true,
//         'code' => $code,
//         'message' => $message,
//         'data' => $data,
//     ],
//     $code
// );
// }

/**
 * Send error response to the request.
 *
 * @param  array  $error
 * @param  int  $code
 * @param  string  $message
 * @return \Illuminate\Http\JsonResponse
 */


function errorResponse($error = [],$code = 200, $message = null)
{
    if (!$message) {
        $message = __('httpmessages.500');
    }
    return response()->json(
        [
            'status' => [
                'success' => false,
                'code' => $code,
                'message' => $message,
            ],
            'error' => $error,
        ]
    );
}