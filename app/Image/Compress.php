<?php

namespace App\Image;

use Image;

class Compress
{

    public function resizeImage($image)
    {
    
       $data['image'] = time().'.'.$image->getClientOriginalExtension();

        $destinationpath = 'compress';
        $img = Image::make($image->getRealPath());
        $item = $img->resize(100,100, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationpath.'/'.$data['image']);
        
        // $data['image'] = $item->save($destinationpath.'/'.$data['image']);
        return $destinationpath.'/'.$data['image'];

    }
}