<?php
namespace App\Cvs;

use Illuminate\Support\Facades\Facade;

class CompressFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'compress';
    }
}