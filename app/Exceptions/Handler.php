<?php

namespace App\Exceptions;

use Error;
use ErrorException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use League\Config\Exception\ValidationException;
use Symfony\Component\CssSelector\Exception\SyntaxErrorException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    public function render($request, Throwable $exception)
    {
        if ($request->expectsJson()){

            if ($exception instanceof ModelNotFoundException){
                return response([
                    'errors'=> 'Object Not Found',
                    'code'=>404
                ], 404);
            }elseif ($exception instanceof NotFoundHttpException){
                return response([
                    'errors'=> 'Route Not Found',
                    'code'=>404
                ], 404);
            }
            elseif ($exception instanceof AuthenticationException){
                return response([
                    'errors'=> 'Unauthenticated',
                    'code'=>401
                ], 401);
            }
            elseif ($exception instanceof AuthorizationException){
                return response([
                    'errors'=> 'Forbidden',
                    'code'=>403
                ], 403);
            }
            elseif (($exception instanceof ErrorException) || ($exception instanceof  ServerException)){
                return response([
                    'errors'=> 'Server Error',
                    'code'=>500
                ], 500);
            }
            elseif ($exception instanceof  Error){
                return response([
                    'errors'=> 'Server Error',
                    'code'=>500
                ], 500);
            }
          
        }

        return parent::render($request, $exception);
        
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    // public function render($request, Exception $exception)
    // {
    //     if ($exception instanceof ModelNotFoundException && $request->wantsJson()) {
    //         return response()->json(['message' => 'Not Found!'], 404);
    //     }

    //     return parent::render($request, $exception);
    // }
}
