<?php


namespace App\Providers;


use App\CRUD\Repositories\BaseCRUDRepository;
use App\CRUD\Repositories\BaseCRUDRepositoryInterface;
use App\Repositories\AuthRepository;
use App\Repositories\AuthRepositoryInterface;
use App\Repositories\CategoryRepository;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\CMS\CmsSlideRepository;
use App\Repositories\CMS\CmsSlideRepositoryInterface;
use App\Repositories\CMS\CmsSliderRepository;
use App\Repositories\CMS\CmsSliderRepositoryInterface;
use App\Repositories\CMS\FaqRepository;
use App\Repositories\CMS\FaqRepositoryInterface;
use App\Repositories\CMS\NewsCategoryRepository;
use App\Repositories\CMS\NewsCategoryRepositoryInterface;
use App\Repositories\CMS\NewsRepository;
use App\Repositories\CMS\NewsRepositoryInterface;
use App\Repositories\CMS\PageRepository;
use App\Repositories\CMS\PageRepositoryInterface;
use App\Repositories\CMS\SeoSettingRepository;
use App\Repositories\CMS\SeoSettingRepositoryInterface;
use App\Repositories\CMS\SiteLogoRepository;
use App\Repositories\CMS\SiteLogoRepositoryInterface;
use App\Repositories\CMS\UserSettingRepository;
use App\Repositories\CMS\UserSettingRepositoryInterface;
use App\Repositories\CommonSettingRepositoryInterface;
use App\Repositories\CommonSettingRepository;
use App\Repositories\ContactRepository;
use App\Repositories\ContactRepositoryInterface;
use App\Repositories\PermissionRepository;
use App\Repositories\PermissionRepositoryInterface;
use App\Repositories\ProductRepository;
use App\Repositories\ProductRepositoryInterface;
use App\Repositories\ProductSettingRepository;
use App\Repositories\ProductSettingRepositoryInterface;
use App\Repositories\RoleRepository;
use App\Repositories\RoleRepositoryInterface;
use App\Repositories\TimerRepository;
use App\Repositories\TimerRepositoryInterface;
use App\Repositories\UserRepository;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            BaseCRUDRepositoryInterface::class,
            BaseCRUDRepository::class
        );
        $this->app->bind(
            PermissionRepositoryInterface::class,
            PermissionRepository::class
        );
        $this->app->bind(
            RoleRepositoryInterface::class,
            RoleRepository::class
        );
        $this->app->bind(
            AuthRepositoryInterface::class,
            AuthRepository::class
        );
        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class
        );
        $this->app->bind(
            TimerRepositoryInterface::class,
            TimerRepository::class
        );
        $this->app->bind(
            CategoryRepositoryInterface::class,
            CategoryRepository::class
        );
        $this->app->bind(
            ProductRepositoryInterface::class,
            ProductRepository::class
        );
        $this->app->bind(
            ProductSettingRepositoryInterface::class,
            ProductSettingRepository::class
        );


        //CMS
        $this->app->bind(
            PageRepositoryInterface::class,
            PageRepository::class
        );
        $this->app->bind(
            SeoSettingRepositoryInterface::class,
            SeoSettingRepository::class
        );
        $this->app->bind(
            CmsSlideRepositoryInterface::class,
            CmsSlideRepository::class
        );
        $this->app->bind(
            CmsSliderRepositoryInterface::class,
            CmsSliderRepository::class
        );
        $this->app->bind(
            FaqRepositoryInterface::class,
            FaqRepository::class
        );
        $this->app->bind(
            NewsRepositoryInterface::class,
            NewsRepository::class
        );
        $this->app->bind(
            NewsCategoryRepositoryInterface::class,
            NewsCategoryRepository::class
        );
        $this->app->bind(
            SiteLogoRepositoryInterface::class,
            SiteLogoRepository::class
        );
        $this->app->bind(
            UserSettingRepositoryInterface::class,
            UserSettingRepository::class
        );
        $this->app->bind(
            ContactRepositoryInterface::class,
            ContactRepository::class
        );
    }
}
