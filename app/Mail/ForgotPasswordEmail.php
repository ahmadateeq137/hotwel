<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $forgot;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($forgot)
    {
        $this->forgot = $forgot;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd(public_path());
        return $this->from(env('MAIL_FROM_ADDRESS'))
            ->view('mails.forgot')
            // ->text('mails.prescription_plain')
            // ->attach(public_path('/images') . '/logo.png', [
            //     'as' => 'logo.png',
            //     'mime' => 'image/png',
            // ])
            ;
    }
}
