<?php

namespace App\Repositories;

use App\CRUD\Repositories\BaseCRUDRepository;
use App\Models\Timer;

class TimerRepository extends BaseCRUDRepository implements TimerRepositoryInterface
{
    public function setup()
    {
        $this->setModel(Timer::class);

        $this->setIndexWiths([]);
        $this->setShowWiths([]);

        $this->setRelated([]);
    }

}
