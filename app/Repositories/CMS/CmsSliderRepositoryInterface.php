<?php

namespace App\Repositories\CMS;

use App\Models\CmsSlider;

interface CmsSliderRepositoryInterface
{
    /**
     * Get's all CmsSlider.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage);
     /**
     * Get's all related data for filling form.
     *
     * @return mixed
     */
    public function related();

    /**
     * Get's a CmsSlider by it's ID
     *
     * @param string
     */
    public function get($id);

    /**
     * Creates a CmsSlider.
     *
     * @param array
     */
    public function create(array $data);

    /**
     * Updates a CmsSlider.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data);

    /**
     * Deletes a CmsSlider.
     *
     * @param int
     */
    public function delete($id);
}
