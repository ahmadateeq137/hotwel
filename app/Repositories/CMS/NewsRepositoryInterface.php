<?php

namespace App\Repositories\CMS;

use App\Models\News;

interface NewsRepositoryInterface
{
    /**
     * Get's all Newss.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage);


        /**
     * Get's all related data for filling form.
     *
     * @return mixed
     */
    public function related();

    /**
     * Get's a News by it's ID
     *
     * @param string
     */
    public function get($id);

    /**
     * Creates a News.
     *
     * @param array
     */
    public function create(array $data);

    /**
     * Updates a News.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data);

    /**
     * Deletes a News.
     *
     * @param int
     */
    public function delete($id);
}
