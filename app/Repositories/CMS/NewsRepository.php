<?php

namespace App\Repositories\CMS;

use App\Models\CMS\News;

class NewsRepository implements NewsRepositoryInterface
{
    /**
     * @var News
     */
    private $model;

    /**
     * NewsRepository constructor.
     *
     * @param News $model
     */
    public function __construct(News $model)
    {
        $this->model = $model;
    }

    /**
     * Get's all News.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage)
    {
        return $this->model->latest()->paginate($perPage);

    }
     /**
     * Get's all related data.
     *
     * @param int
     * @return mixed
     */
    public function related()
    {
        $data = [];
        $data['newscategory'] = $this->model->newscategory()->getModel()->select('id','name')->get();
        return $data;
    }

    /**
     * Get's a News by it's ID
     *
     * @param int
     * @return collection
     */
    public function get($id)
    {
        return $this->model->with('newscategory')->findorFail($id);
    }

    /**
     * Creates a News.
     *
     * @param array
     */
    public function create(array $data)
    {
        $item = $this->model->create($data);
        return $item;
    }

    /**
     * Deletes a News.
     *
     * @param int
     */
    public function delete($id)
    {
        $item = $this->model->destroy($id);
        return $item;
    }

    /**
     * Updates a News.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data)
    {
        $item = $this->get($id);
        $item->update($data);
        return $item;
    }
}
