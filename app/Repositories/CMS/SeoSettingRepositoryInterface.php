<?php

namespace App\Repositories\CMS;

use App\Models\SeoSetting;

interface SeoSettingRepositoryInterface
{
    /**
     * Get's all SeoSettings.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage);


        /**
     * Get's all related data for filling form.
     *
     * @return mixed
     */
    public function related();

    /**
     * Get's a SeoSetting by it's ID
     *
     * @param string
     */
    public function get($id);

    /**
     * Creates a SeoSetting.
     *
     * @param array
     */
    public function create(array $data);

    /**
     * Updates a SeoSetting.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data);

    /**
     * Deletes a SeoSetting.
     *
     * @param int
     */
    public function delete($id);
}
