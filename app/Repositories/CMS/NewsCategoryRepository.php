<?php

namespace App\Repositories\CMS;

use App\Models\CMS\NewsCategory;

class NewsCategoryRepository implements NewsCategoryRepositoryInterface
{
    /**
     * @var NewsCategory
     */
    private $model;

    /**
     * NewsCategoryRepository constructor.
     *
     * @param NewsCategory $model
     */
    public function __construct(NewsCategory $model)
    {
        $this->model = $model;
    }

    /**
     * Get's all NewsCategory.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage)
    {
        return $this->model->latest()->paginate($perPage);

    }
     /**
     * Get's all related data.
     *
     * @param int
     * @return mixed
     */
    public function related()
    {
        $data = [];
        return $data;
    }

    /**
     * Get's a NewsCategory by it's ID
     *
     * @param int
     * @return collection
     */
    public function get($id)
    {
        return $this->model->latest()->with('news')->findorFail($id);

    }

    /**
     * Creates a NewsCategory.
     *
     * @param array
     */
    public function create(array $data)
    {
        $item = $this->model->create($data);
        return $item;
    }

    /**
     * Deletes a NewsCategory.
     *
     * @param int
     */
    public function delete($id)
    {
        $item = $this->model->destroy($id);
        return $item;
    }

    /**
     * Updates a NewsCategory.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data)
    {
        $item = $this->get($id);
        $item->update($data);
        return $item;
    }
}
