<?php

namespace App\Repositories\CMS;

use App\Models\CmsSlide;

interface CmsSlideRepositoryInterface
{
    /**
     * Get's all CmsSlide.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage);

        /**
     * Get's all related data for filling form.
     *
     * @return mixed
     */
    public function related();

    /**
     * Get's a CmsSlide by it's ID
     *
     * @param string
     */
    public function get($id);

    /**
     * Creates a CmsSlide.
     *
     * @param array
     */
    public function create(array $data);

    /**
     * Updates a CmsSlide.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data);

    /**
     * Deletes a CmsSlide.
     *
     * @param int
     */
    public function delete($id);


}
