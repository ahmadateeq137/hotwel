<?php

namespace App\Repositories\CMS;

use App\Models\SeoSetting;

class SeoSettingRepository implements SeoSettingRepositoryInterface
{
    /**
     * @var SeoSetting
     */
    private $model;

    /**
     * SeoSettingRepository constructor.
     *
     * @param SeoSetting $model
     */
    public function __construct(SeoSetting $model)
    {
        $this->model = $model;
    }

    /**
     * Get's all SeoSetting.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage)
    {
        return $this->model->latest()->paginate($perPage);
        // return $this->model->latest()->get('SeoSetting');

    }
     /**
     * Get's all related data.
     *
     * @param int
     * @return mixed
     */
    public function related()
    {
        $data = [];
        return $data;
    }

    /**
     * Get's a SeoSetting by it's ID
     *
     * @param int
     * @return collection
     */
    public function get($id)
    {
        // return $this->model->findorFail($id);
        return $this->model->with(['pages'])->findorFail($id);


    }

    /**
     * Creates a SeoSetting.
     *
     * @param array
     */
    public function create(array $data)
    {
        $item = $this->model->create($data);
        return $item;
    }

    /**
     * Deletes a SeoSetting.
     *
     * @param int
     */
    public function delete($id)
    {
        $item = $this->model->destroy($id);
        return $item;
    }

    /**
     * Updates a SeoSetting.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data)
    {
        $item = $this->get($id);
        $item->update($data);
        return $item;
    }
}
