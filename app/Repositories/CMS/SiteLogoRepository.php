<?php

namespace App\Repositories\CMS;

use App\Models\CMS\SiteLogo;

class SiteLogoRepository implements SiteLogoRepositoryInterface
{
    /**
     * @var SiteLogo
     */
    private $model;

    /**
     * SiteLogoRepository constructor.
     *
     * @param SiteLogo $model
     */
    public function __construct(SiteLogo $model)
    {
        $this->model = $model;
    }

    /**
     * Get's all SiteLogo.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage)
    {
        return $this->model->latest()->paginate($perPage);

    }
     /**
     * Get's all related data.
     *
     * @param int
     * @return mixed
     */
    public function related()
    {
        $data = [];
        return $data;
    }

    /**
     * Get's a SiteLogo by it's ID
     *
     * @param int
     * @return collection
     */
    public function get($id)
    {
        return $this->model->findorFail($id);

    }

    /**
     * Creates a SiteLogo.
     *
     * @param array
     */
    public function create(array $data)
    {
        $item = $this->model->create($data);
        return $item;
    }

    /**
     * Deletes a SiteLogo.
     *
     * @param int
     */
    public function delete($id)
    {
        $item = $this->model->destroy($id);
        return $item;
    }

    /**
     * Updates a SiteLogo.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data)
    {
        $item = $this->get($id);
        $item->update($data);
        return $item;
    }
}
