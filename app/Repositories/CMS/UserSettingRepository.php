<?php


namespace App\Repositories\CMS;


use App\CRUD\Repositories\BaseCRUDRepository;
use App\Models\UserSetting;


class UserSettingRepository extends BaseCRUDRepository implements UserSettingRepositoryInterface
{
   public function setup()
   {
       $this->setModel(UserSetting::class);

       $this->setIndexWiths(['user']);
       $this->setShowWiths(['user']);

       $this->setRelated(['user']);
   }
}
