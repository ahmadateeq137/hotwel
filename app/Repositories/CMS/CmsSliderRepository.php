<?php

namespace App\Repositories\CMS;

use App\Models\CMS\CmsSlider;

class CmsSliderRepository implements CmsSliderRepositoryInterface
{
    /**
     * @var CmsSlider
     */
    private $model;

    /**
     * CmsSlider Repository constructor.
     *
     * @param CmsSlider $model
     */
    public function __construct(CmsSlider $model)
    {
        $this->model = $model;
    }

    /**
     * Get's all CmsSlider.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage)
    {
        return $this->model->latest()->paginate($perPage);
    }
     /**
     * Get's all related data.
     *
     * @param int
     * @return mixed
     */

    public function related()
    {
        $data = [];
        return $data;
    }

    /**
     * Get's a cmsslider by it's ID
     *
     * @param int
     * @return collection
     */
    public function get($id)
    {
        return $this->model->findorFail($id);
    }

    /**
     * Creates a CmsSlider.
     *
     * @param array
     */
    public function create(array $data)
    {
        $item = $this->model->create($data);
        return $item;
    }

    /**
     * Deletes a CmsSlider.
     *
     * @param int
     */
    public function delete($id)
    {
        $item = $this->model->destroy($id);
        return $item;
    }

    /**
     * Updates a CmsSlider.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data)
    {
        $item = $this->get($id);
        $item->update($data);
        return $item;
    }
}
