<?php

namespace App\Repositories\CMS;

use App\Models\SiteLogo;

interface SiteLogoRepositoryInterface
{
    /**
     * Get's all SiteLogos.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage);


        /**
     * Get's all related data for filling form.
     *
     * @return mixed
     */
    public function related();

    /**
     * Get's a SiteLogo by it's ID
     *
     * @param string
     */
    public function get($id);

    /**
     * Creates a SiteLogo.
     *
     * @param array
     */
    public function create(array $data);

    /**
     * Updates a SiteLogo.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data);

    /**
     * Deletes a SiteLogo.
     *
     * @param int
     */
    public function delete($id);
}
