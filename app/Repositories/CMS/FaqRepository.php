<?php

namespace App\Repositories\CMS;

use App\Models\CMS\Faq;

class FaqRepository implements FaqRepositoryInterface
{
    /**
     * @var Faq
     */
    private $model;

    /**
     * FaqRepository constructor.
     *
     * @param Faq $model
     */
    public function __construct(Faq $model)
    {
        $this->model = $model;
    }

    /**
     * Get's all Faq.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage)
    {
        return $this->model->latest()->paginate($perPage);

    }
     /**
     * Get's all related data.
     *
     * @param int
     * @return mixed
     */
    public function related()
    {
        $data = [];
        return $data;
    }

    /**
     * Get's a Faq by it's ID
     *
     * @param int
     * @return collection
     */
    public function get($id)
    {
        return $this->model->findorFail($id);

    }

    /**
     * Creates a Faq.
     *
     * @param array
     */
    public function create(array $data)
    {
        $item = $this->model->create($data);
        return $item;
    }

    /**
     * Deletes a Faq.
     *
     * @param int
     */
    public function delete($id)
    {
        $item = $this->model->destroy($id);
        return $item;
    }

    /**
     * Updates a Faq.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data)
    {
        $item = $this->get($id);
        $item->update($data);
        return $item;
    }
}
