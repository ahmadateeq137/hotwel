<?php

namespace App\Repositories\CMS;

use App\Models\CMS\CmsSlide;

class CmsSlideRepository implements CmsSlideRepositoryInterface
{
    /**
     * @var CmsSlide
     */
    private $model;

    /**
     * CmsSlide Repository constructor.
     *
     * @param CmsSlide $model
     */
    public function __construct(CmsSlide $model)
    {
        $this->model = $model;
    }

    /**
     * Get's all CmsSlide.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage)
    {
        return $this->model->latest()->paginate($perPage);
    }
    public function related()
    {
        $data = [];
        $data['cmsSlider'] = $this->model->cmsSlider()->getModel()->select('id','name')->get();
        return $data;
    }
    /**
     * Get's a CmsSlide by it's ID
     *
     * @param int
     * @return collection
     */
    public function get($id)
    {
        return $this->model->with(['cmsSlider'])->findorFail($id);

    }

    /**
     * Creates a CmsSlide.
     *
     * @param array
     */
    public function create(array $data)
    {
        $item = $this->model->create($data);
        return $item;
    }

    /**
     * Deletes a CmsSlide.
     *
     * @param int
     */
    public function delete($id)
    {
        $item = $this->model->destroy($id);
        return $item;
    }

    /**
     * Updates a CmsSlide.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data)
    {
        $item = $this->get($id);
        $item->update($data);
        return $item;
    }
}
