<?php

namespace App\Repositories\CMS;

use App\Models\NewsCategory;

interface NewsCategoryRepositoryInterface
{
    /**
     * Get's all NewsCategorys.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage);


        /**
     * Get's all related data for filling form.
     *
     * @return mixed
     */
    public function related();

    /**
     * Get's a NewsCategory by it's ID
     *
     * @param string
     */
    public function get($id);

    /**
     * Creates a NewsCategory.
     *
     * @param array
     */
    public function create(array $data);

    /**
     * Updates a NewsCategory.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data);

    /**
     * Deletes a NewsCategory.
     *
     * @param int
     */
    public function delete($id);
}
