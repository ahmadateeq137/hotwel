<?php

namespace App\Repositories\CMS;

use App\Models\CMS\Page;

interface PageRepositoryInterface
{
    /**
     * Get's all Pages.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage);


        /**
     * Get's all related data for filling form.
     *
     * @return mixed
     */
    public function related();

    /**
     * Get's a Page by it's ID
     *
     * @param string
     */
    public function get($id);

    /**
     * Creates a Page.
     *
     * @param array
     */
    public function create(array $data);

    /**
     * Updates a Page.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data);

    /**
     * Deletes a Page.
     *
     * @param int
     */
    public function delete($id);
}
