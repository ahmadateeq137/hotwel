<?php


namespace App\Repositories\CMS;


use App\CRUD\Repositories\BaseCRUDRepository;
use App\Models\CMS\Page;


class PageRepository extends BaseCRUDRepository implements PageRepositoryInterface
{
   public function setup()
   {
       $this->setModel(Page::class);

       $this->setIndexWiths([]);
       $this->setShowWiths([]);

       $this->setRelated([]);
   }
}
