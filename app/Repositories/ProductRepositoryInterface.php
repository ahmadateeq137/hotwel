<?php

namespace App\Repositories;

use App\Models\Product;

interface ProductRepositoryInterface
{
    /**
     * Get's all Product.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage);


        /**
     * Get's all related data for filling form.
     *
     * @return mixed
     */
    public function related();

    /**
     * Get's a Product by it's ID
     *
     * @param string
     */
    public function get($id);

    /**
     * Creates a Product.
     *
     * @param array
     */
    public function create(array $data);

    /**
     * Updates a Product.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data);

    /**
     * Deletes a Product.
     *
     * @param int
     */
    public function delete($id);
}
