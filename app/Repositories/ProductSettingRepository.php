<?php

namespace App\Repositories;

use App\CRUD\Repositories\BaseCRUDRepository;
use App\Models\ProductSetting;

class ProductSettingRepository extends BaseCRUDRepository implements ProductSettingRepositoryInterface
{
    public function setup()
    {
        $this->setModel(ProductSetting::class);

        $this->setIndexWiths([]);
        $this->setShowWiths([]);

        $this->setRelated([]);
    }

}
