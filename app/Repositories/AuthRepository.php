<?php


namespace App\Repositories;

use App\Mail\ForgotPasswordEmail;
use Illuminate\Validation\ValidationException;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Hashing\Hasher as Hash;
use App\Mail\Reset;
use App\Mail\Welcome;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;

class AuthRepository implements AuthRepositoryInterface
{
    private $model;

    /**      
     * @var Hash
     */
    private $hash;
    public function __construct(User $user, Hash $hash)
    {
        $this->model = $user;
        $this->hash = $hash;
    }

    public function attempt($credentials)
    {
        $user = $this->model->where('phone_number',$credentials['phone_number'])->first();
        if (!$user) {
            $error = ValidationException::withMessages([
                'phone_number' => ['The selected phone number is not valid'],
            ]);
            throw $error;
        }
        $passwordMatched = $this->hash->check($credentials['password'], $user->password);
        if (!$passwordMatched) {
            $error = ValidationException::withMessages([
                'password' => ['The selected password is not valid'],
            ]);
            throw $error;
        }
        return $user;
    }

     public function forgot($data)
    {
        $application = $data['app_type'] ?? null;
        if ($application == "mobile") {
            $user = User::where('phone_number',$data['phone_number'])->get('email')->first();
            if ($user) {
                $otp = mt_rand(100000, 999999);
                $objDemo = new \stdClass();
                $objDemo->otp = $otp;
                Mail::to($user->email)->send(new ForgotPasswordEmail($objDemo));


                // $user->notify(new ResetPasswordNotification($otp, 'mobile'));
                DB::table('password_resets')
                    ->updateOrInsert(
                        ['phone_number' => $data['phone_number']],
                        ['token' => $otp]
                    );
            } else {
                return errorResponse(422, 'Sorry! No such email found.');
            }
        } else if ($application == "web") {
            $this->broker()->sendResetLink($data);
        }
        return successResponse(null, 200, 'Successfully! Reset password email is sent.');
    }

    public function broker()
    {
        return Password::broker();
    }

    /**
     * @param $data
     */
    public function optVerify($data)
    {
        // $item = DB::table('password_resets')->where([
        //     'email' => $data['email'],
        //     'token' => $data['token']
        // ])->first();
        // if (!$item) {
        //     return abort(422,'Token does not match');
        // }else{
        //     return successResponse(null,200,'Successfully! Otp is verified.');
        // }
    }

    public function resetPassword($data)
    {
        $item = DB::table('password_resets')->where([
            'token' => $data['token']
        ])->first();
        if (!$item) {
            return abort(422,'Token does not match');
        }else{
            $data['password'] = $this->hash->make($data['password']);
            $pass = [
                'password' => $data['password']
            ];

        $update = User::where('phone_number',$data['phone_number'])->get('email')->first();
         $update->whereEmail($update->email)->update($pass);
        if ($update) {
            $objDemo = new \stdClass();
            $receiver = $update->email;
            $objDemo->receiver = $receiver;
            $sender = 'HotWell';
            $objDemo->sender = $sender;
                Mail::to($receiver)->send(new Welcome($objDemo));
            return  successResponse(null, 200, 'Successfully! Password is reset.');
        } else {
            return errorResponse();
        }
            // return successResponse(null,200,'Successfully! Otp is verified.');
        }
        
    }
}
