<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\ProductRepositoryInterface;

class ProductRepository implements ProductRepositoryInterface
{
    /**
     * @var Product
     */
    private $model;

    /**
     * ProductRepository constructor.
     *
     * @param Product $model
     */
    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    /**
     * Get's all Product.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage)
    {
        return $this->model->latest()->paginate($perPage);

    }
     /**
     * Get's all related data.
     *
     * @param int
     * @return mixed
     */
    public function related()
    {
        $data = [];
        return $data;
    }

    /**
     * Get's a Product by it's ID
     *
     * @param int
     * @return collection
     */
    public function get($id)
    {
        return $this->model->findorFail($id);

    }

    /**
     * Creates a Product.
     *
     * @param array
     */
    public function create(array $data)
    {
        $item = $this->model->create($data);
        return $item;
    }

    /**
     * Deletes a Product.
     *
     * @param int
     */
    public function delete($id)
    {
        $item = $this->model->destroy($id);
        return $item;
    }

    /**
     * Updates a Product.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data)
    {
        $item = $this->get($id);
        $item->update($data);
        return $item;
    }
}
