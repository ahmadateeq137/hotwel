<?php

namespace App\Repositories;

use App\Models\Category;

interface CategoryRepositoryInterface
{
    /**
     * Get's all Category.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage);


        /**
     * Get's all related data for filling form.
     *
     * @return mixed
     */
    public function related();

    /**
     * Get's a Category by it's ID
     *
     * @param string
     */
    public function get($id);

    /**
     * Creates a Category.
     *
     * @param array
     */
    public function create(array $data);

    /**
     * Updates a Category.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data);

    /**
     * Deletes a Category.
     *
     * @param int
     */
    public function delete($id);
}
