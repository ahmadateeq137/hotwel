<?php


namespace App\Repositories;


use App\CRUD\Repositories\BaseCRUDRepository;
use App\Models\Contact;


class ContactRepository extends BaseCRUDRepository implements ContactRepositoryInterface
{
    public function setup()
    {
        $this->setModel(Contact::class);

        $this->setIndexWiths(['user']);
        $this->setShowWiths(['user']);

        $this->setRelated(['user']);
    }
   
}
