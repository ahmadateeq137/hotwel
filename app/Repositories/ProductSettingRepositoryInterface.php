<?php

namespace App\Repositories;


interface ProductSettingRepositoryInterface
{
    /**
     * Get's all Settings.
     *
     * @param int
     * @return mixed
     */
    public function paginate($perPage);


        /**
     * Get's all related data for filling form.
     *
     * @return mixed
     */
    public function related();

    /**
     * Get's a Setting by it's ID
     *
     * @param string
     */
    public function get($id);

    /**
     * Creates a Setting.
     *
     * @param array
     */
    public function create(array $data);

    /**
     * Updates a Setting.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data);

    /**
     * Deletes a Setting.
     *
     * @param int
     */
    public function delete($id);

}
