<?php

namespace App\Resources;

use App\Resources\BaseResource;

class RoleResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                'id' => $this->id,
                'name' => $this->name,
                'permissions' => PermissionResource::collection($this->whenLoaded('permissions')),

        ];
    }
}
