<?php

namespace App\Resources;


use App\Resources\BaseResource;

class TimerResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'gass'=>$this->gass,
            'electricity'=>$this->electricity,
            'start_time'=>$this->start_time,
            'end_time'=>$this->end_time,
            'auto'=>$this->auto,
            'temperature'=>$this->temperature,
            'product_id'=>$this->product_id,
            'monday'=>$this->monday,
            'tuesday'=>$this->tuesday,
            'wednesday'=>$this->wednesday,
            'thursday'=>$this->thursday,
            'friday'=>$this->friday,
            'saturday'=>$this->saturday,
            'sunday'=>$this->sunday,
            'status'=>$this->status,
            'gas_status'=>$this->gas_status,
            'electricity_status'=>$this->electricity_status,
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at,
        ];
    }
}
