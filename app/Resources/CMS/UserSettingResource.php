<?php

 

namespace App\Resources\CMS;

use Illuminate\Http\Resources\Json\JsonResource;

class UserSettingResource  extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'key' => $this->key,
            'value' => $this->value,
            'user_id' => new UserResource($this->whenLoaded('user')),
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at,
        ];
    }
}
