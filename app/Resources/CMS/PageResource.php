<?php

namespace App\Resources\CMS;

use App\Resources\BaseResource;

class PageResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug'=>$this->slug,
            'type'=>$this->type,
            'target' => $this->target,
            'external_link' => $this->external_link,
            'description1'=>$this->description1,
            'description1_heading'=>$this->description1_heading,
            'description1_image' => $this->description1_image,
            'description2' => $this->description2,
            'description2_heading'=>$this->description2_heading,
            'description2_image'=>$this->description2_image,
            'menu' => $this->menu,
            'featured_image' => $this->featured_image,
            'status'=>$this->status,
            'order_by'=>$this->order_by,
            'seo_setting_id' => new SeoSettingResource($this->whenLoaded('seoSetting')),
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at,
        ];
    }
}
