<?php

 

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource  extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name'=>$this->last_name,
            'phone_number'=>$this->phone_number,
            'verification_token'=>$this->verification_token,
            'email'=>$this->email,
            'role' => RoleResource::collection($this->whenLoaded('role')),
            'image'=>$this->image,
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at,
        ];
    }
}
