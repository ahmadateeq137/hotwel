<?php

namespace App\Resources;

use App\Resources\BaseResource as ResourcesBaseResource;
use Gmz\Common\Resources\BaseResource;

class PermissionResource extends ResourcesBaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name' => $this->name,
            'guard_name'=>$this->guard_name
            // 'permissions' => $this->permissions,
        ];
    }
}
