<?php

namespace App\Http\Requests;

use App\Models\User;

class CategoryStoreRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'name'=>'required',
            'image'=>'nullable|mimes:jpg,jpeg,png',
            'description'=>'nullable',
            'user_id'=> 'nullable|exists:' . User::class . ',id',
        ];
    }
}