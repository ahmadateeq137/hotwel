<?php

namespace App\Http\Requests;

use App\Models\User;

class ContactUpdateRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'first_name'=>'nullable|string|max:100',
            'last_name'=>'nullable|string|max:100',
            'phone'=>'nullable',
            'email'=>'nullable|email',
            'description'=>'required|max:1000',
            'user_id'=> 'nullable|exists:' . User::class . ',id',
        ];
    }
}