<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;

class ProductSettingStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'gass'=>'nullable|boolean',
            'electricity'=>'nullable|boolean',
            'gas_status'=>'nullable|boolean',
            'electricity_status'=>'nullable|boolean',
            'temperature_status'=>'nullable|boolean',
            // 'timer_status'=>'nullable',
            'auto'=>'nullable|boolean',
            'temperature'=>'nullable',
            'status'=>'nullable|boolean',
            'product_id'=> 'required|exists:' . Product::class . ',id',
        ];
    }
}
