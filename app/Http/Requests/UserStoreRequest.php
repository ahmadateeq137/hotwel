<?php

namespace App\Http\Requests;


use App\Models\User;

class UserStoreRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'first_name'=>'required|string|max:100',
            'last_name'=>'required|string|max:100',
            'password'=>'required|min:6|max:100',
            'verification_token'=>'nullable',
            'phone_number'=>'required',
            'email'=>'required|email|unique:'.User::class.',email',
            'role' =>'sometimes|string|exists:roles,name',
            'image'=>'nullable|mimes:jpg,jpeg,png'
        ];
    }
}