<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;

class TimerStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:190',
            'monday'=>'nullable|boolean',
            'tuesday'=>'nullable|boolean',
            'wednesday'=>'nullable|boolean',
            'thursday'=>'nullable|boolean',
            'friday'=>'nullable|boolean',
            'saturday'=>'nullable|boolean',
            'sunday'=>'nullable|boolean',
            'start_time'=>'required|date_format:H:i',
            'end_time'=>'required|after:start_time|date_format:H:i',
            'gass'=>'nullable|boolean',
            'electricity'=>'nullable|boolean',
            'auto'=>'nullable|boolean',
            'temperature'=>'required',
            'status'=>'nullable|boolean',
            'product_id'=> 'required|exists:' . Product::class . ',id',
        ];
    }
}
