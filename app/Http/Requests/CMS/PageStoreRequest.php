<?php

namespace App\Http\Requests\CMS;
use App\Models\SeoSetting;
use Illuminate\Foundation\Http\FormRequest;

class PageStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'seo_setting_id' => 'exists:' . SeoSetting::class . ',id',
            'title' => 'required|string|max:190|unique:pages,title',
            'slug' => 'string',
            'type' => 'required|string',
            'target' => 'string',
            'external_link' => '',
            'description1' => 'nullable',
            'description1_heading' => 'required',
            'description1_image' => 'nullable',
            'description2' => 'nullable',
            'description2_heading' => '',
            'description2_image' => 'nullable',
            'menu' => 'required',
            'featured_image' => 'nullable',
            'status' => 'required|in:active,inactive',
            'order_by' => '',

        ];
    }
}
