<?php

namespace App\Http\Requests\CMS;

use App\Http\Requests\BaseRequest;
use App\Models\User;

class UserSettingStoreRequest extends BaseRequest
{

    public function rules()
    {
        return [
            'key'=>'required|max:190',
            'value'=>'required|max:190',
            'user_id'=> 'nullable|exists:' . User::class . ',id',
        ];
    }
}
