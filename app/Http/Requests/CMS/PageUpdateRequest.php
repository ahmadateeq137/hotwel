<?php

namespace App\Http\Requests\CMS;

use App\Models\SeoSetting;
use Illuminate\Foundation\Http\FormRequest;

class PageUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'seo_setting_id' => 'exists:' . SeoSetting::class . ',id',
            'title' => 'nullable|max:190',
            'slug' => 'nullable',
            'type' => 'nullable',
            'target' => 'nullable',
            'external_link' => '',
            'description1' => 'nullable',
            'description1_heading' => 'nullable',
            'description1_image' => 'nullable',
            'description2' => 'nullable',
            'description2_heading' => 'nullable',
            'description2_image' => 'nullable',
            'menu' => 'nullable',
            'featured_image' => 'nullable',
            'status' => 'in:active,inactive',
            'order_by' => 'nullable',
        ];
    }
}
