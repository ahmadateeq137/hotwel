<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class CmsSliderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:190|unique:cms_sliders',
            'description' => 'string',
            'status' => 'required|in:active,inactive',
            'cover_photo' => 'image',
            'featured' => 'in:0,1',
        ];
    }
}
