<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class SeoSettingStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           
            'meta_title' => 'string|unique:seo_settings',
            'meta_description' => 'string',
            'static_page_id' => '',
            'custom_page_id' => '',
            'meta_keywords' => 'string',
            'featured_image' => '',
            'facebook_link' => 'url',
            'facebook_title' => 'string', 
             'facebook_description' => 'string',
             'twitter_link' => 'url',
            'twitter_title' => 'string', 
             'twitter_description' => 'string',
           
        ];
    }
}
