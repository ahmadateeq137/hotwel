<?php

namespace App\Http\Requests\CMS;

use App\Models\CMS\Page;
use Illuminate\Foundation\Http\FormRequest;

class SeoSettingUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'meta_title' => 'string',
            'meta_description' => '',
            'static_page_id' => '',
            'custom_page_id' => 'string',
            'meta_keywords' => 'string',
            'featured_image' => '',
            'facebook_link' => 'url',
            'facebook_title' => 'string', 
             'facebook_description' => 'string',
             'twitter_link' => 'url',
            'twitter_title' => 'string', 
             'twitter_description' => 'string',
        ];
    }
}
