<?php

namespace App\Http\Requests\CMS;
use App\Models\NewsCategory;

use Illuminate\Foundation\Http\FormRequest;

class NewsStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'news_category_id' => 'required|exists:' . NewsCategory::class . ',id',
            'title' => 'required|string|max:190|unique:news',
            'category_name' => 'required|string|max:190',
            'description' => 'required|string',
            'featured_image' => 'image',
            'image' => 'image',
            'status' => 'required|in:active,inactive',
            'featured' => 'in:0,1',
            'meta_title' => 'required',
            'meta_description' => 'required',
            'meta_keywords' => 'required',

        ];
    }
}
