<?php

namespace App\Http\Requests\CMS;

use App\Models\CmsSlider;
use Illuminate\Foundation\Http\FormRequest;

class CmsSlideUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'string',
            'cms_slider_id' => 'required|exists:' . CmsSlider::class . ',id',
            'rating' => 'string',
            'description' => 'string',
            'amount' => 'string',
            'date_from' => 'date',
            'date_to' => 'date',
            'image' => 'image',
            'status' => 'required|max:190|in:active,inactive',
            'featured' => 'in:0,1',
        ];
    }
}
