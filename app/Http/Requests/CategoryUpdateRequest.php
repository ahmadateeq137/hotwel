<?php

namespace App\Http\Requests;

use App\Models\User;

class CategoryUpdateRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'name'=>'required',
            'image'=>'nullable',
            'description'=>'nullable',
            'user_id'=> 'nullable|exists:' . User::class . ',id',
        ];
    }
}