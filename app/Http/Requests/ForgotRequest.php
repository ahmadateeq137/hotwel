<?php

namespace App\Http\Requests;

use App\Models\User;

class ForgotRequest extends BaseRequest
{

    public function rules()
    {
        return [
            'phone_number'=>'required|exists:'.User::class.',phone_number',
            'app_type'=>'nullable|in:mobile,web'
        ];
    }
}
