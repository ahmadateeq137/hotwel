<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;

class TimerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'name' => 'nullable|max:190',
            'monday'=>'boolean',
            'tuesday'=>'boolean',
            'wednesday'=>'boolean',
            'thursday'=>'boolean',
            'friday'=>'boolean',
            'saturday'=>'boolean',
            'sunday'=>'boolean',
            'start_time'=>'nullable|date_format:H:i',
            'end_time'=>'nullable|after:start_time|date_format:H:i',
            'gass'=>'boolean',
            'electricity'=>'boolean',
            'auto'=>'boolean',
            'temperature'=>'nullable',
            'status'=>'boolean',
            'gas_status'=>'boolean',
            'electricity_status'=>'boolean',
            // 'product_id'=> 'nullable|exists:' . Product::class . ',id',
        ];
    }
}
