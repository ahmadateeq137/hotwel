<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RoleStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'max:190',
                Rule::unique('roles')->where(function ($query) {
                    return $query->where('guard_name', 'user');
                }),
            ],
            'permissions' => 'required',
            'permissions.*' => [
                'required',
                Rule::exists('permissions', 'name')->where(function ($query) {
                    $query->where('guard_name', 'user');
                }),
            ]
        ];
    }
}
