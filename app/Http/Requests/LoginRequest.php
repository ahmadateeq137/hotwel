<?php

namespace App\Http\Requests;

use App\Models\User;

class LoginRequest extends BaseRequest
{

    public function rules()
    {
        return [
            'token'=>'required|exists:password_resets',
            'password' => 'required|string|min:6|confirmed',
            'phone_number'=>'required|exists:'.User::class.',phone_number'
        ];
    }
}
