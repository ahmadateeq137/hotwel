<?php

namespace App\Http\Requests;

use App\Models\Category;
use App\Models\User;

class ProductStoreRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'mac_address'=>'required|min:17|max:17|unique:products,mac_address',
            'auto_shift'=>'boolean',
            'category_id'=> 'required|exists:' . Category::class . ',id',
            'user_id'=> 'nullable|exists:' . User::class . ',id',
        ];
    }
}