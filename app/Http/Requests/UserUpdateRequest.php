<?php

namespace App\Http\Requests;

use App\Models\User;

class UserUpdateRequest extends BaseRequest
{

    public function rules()
    {
        return [
            'first_name'=>'required|string',
            'last_name'=>'required|string',
            'role' => 'sometimes|string|exists:roles,name',
            'image'=>'nullable'
        ];
    }
}
