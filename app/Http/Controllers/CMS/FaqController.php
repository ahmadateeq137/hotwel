<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CMS\Faq;
use App\Resources\CMS\FaqResource;
use App\Resources\CMS\FaqCollection;
use App\Http\Requests\CMS\FaqStoreRequest;
use App\Http\Requests\CMS\FaqUpdateRequest;
use Illuminate\Contracts\Hashing\Hasher as Hash;
use App\Repositories\CMS\FaqRepositoryInterface;

class FaqController extends Controller
{
    /**
     * @var FaqRepositoryInterface
     */
    private $repository;
    /**
     * @var Hash
     */
    private $hash;

    public function __construct(FaqRepositoryInterface $repository, Hash $hash)
    {
        $this->middleware('permission:View Faq')->only('index');
        $this->middleware('permission:Create Faq')->only('store');
        $this->middleware('permission:Update Faq')->only('update');
        $this->middleware('permission:Delete Faq')->only('destroy');

        $this->repository = $repository;
        $this->hash = $hash;
    }

    /**
     * Display a listing of the Faq.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $params = [
            'page' => 1,
            'perPage' => 10,
            'sortField' => 'created_at',
            'sortOrder' => 'desc',
            'searchKeyword' => null,

        ];
        $requestParams = $request->query();
        $params = array_merge($params, $requestParams);
        $query = Faq::orderBy($params['sortField'], $params['sortOrder']);
        if ($params['searchKeyword']) {
            $query = $query->whereLike(['question', 'answer'], $params['searchKeyword']);
        }
        $items = $query->paginate($params['perPage']);
        return successResponse(new FaqCollection($items));
    }
    // {
    //     $params = [
    //         'perPage' => 10
    //     ];
    //     $requestParams = $request->only('perPage');
    //     $params = array_merge($params, $requestParams);
    //     $items = $this->repository->paginate($params['perPage']);
    //     return successResponse(new FaqCollection($items));

    // }
    public function related()
    {
        $items = $this->repository->related();

        return successResponse($items);
    }

    /**
     * Store a newly created Faq in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqStoreRequest $request)
    {
        $data = $request->validated();
        $item = $this->repository->create($data);
        return successResponse($item);
    }

    /**
     * Display the specified Faq.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->repository->get($id);
        return  successResponse(new FaqResource($item));
        // return ($item);

    }

    /**
     * Update the specified Faq in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqUpdateRequest $request, $id)
    {
        $data = $request->validated();
        $item = $this->repository->update($id, $data);
        return successResponse($item);
    }

    /**
     * Remove the specified company contact from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->repository->delete($id);
        return successResponse($item);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->ids;
        $this->model->whereIn('id', $ids)->delete();

        return successResponse();
    }
}
