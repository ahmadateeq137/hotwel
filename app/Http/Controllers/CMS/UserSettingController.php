<?php

namespace App\Http\Controllers\CMS;

use App\Models\CMS\UserSetting;
use Illuminate\Http\Request;
use App\CRUD\Traits\PaginateTrait;
use App\Resources\CMS\UserSettingResource;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMS\UserSettingStoreRequest;
use App\Http\Requests\CMS\UserSettingUpdateRequest;
use App\Repositories\CMS\UserSettingRepositoryInterface;


class UserSettingController extends Controller
{
    use PaginateTrait;

    private $repository;

    public function __construct(UserSettingRepositoryInterface $repository)
    {
        // $this->middleware('permission:View User Setting')->only('index');
        // $this->middleware('permission:Create User Setting')->only('store');
        // $this->middleware('permission:Create User Setting|Update User Setting')->only('related');
        // $this->middleware('permission:Update User Setting')->only('update');
        // $this->middleware('permission:Delete User Setting')->only('destroy');
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $items = UserSetting::where('user_id', auth()->id())->with('user')->orderBy('id', 'desc')->paginate(10);
        return successResponse(UserSettingResource::collection($items));
    }

    public function related()
    {
        $items = $this->repository->related();
        return successResponse($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserSettingStoreRequest $request)
    {
        $data = $request->validated();

        if (!$request['user_id']) {
            $data['user_id'] = auth()->id();
        }
        $item = $this->repository->create($data);

        return successResponse($item);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->repository->get($id);
        return  successResponse(new UserSettingResource($item));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserSettingUpdateRequest $request, $id)
    {
        $data = $request->validated();

        if (!$request->user_id) {
            $request->user_id = auth()->id();
        }
            $item = $this->repository->update($id, $data);

            return successResponse($item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return successResponse();
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->ids;
        $this->model->whereIn('id', $ids)->delete();

        return successResponse();
    }
}
