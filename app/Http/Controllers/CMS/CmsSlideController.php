<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CMS\CmsSlide;
use App\Resources\CMS\CmsSlideResource;
use App\Resources\CMS\CmsSlideCollection;
use App\Http\Requests\CMS\CmsSlideStoreRequest;
use App\Http\Requests\CMS\CmsSlideUpdateRequest;
use Illuminate\Contracts\Hashing\Hasher as Hash;
use App\Repositories\CMS\CmsSlideRepositoryInterface;

class CmsSlideController extends Controller
{
    /**
     * @var CmsSlideRepositoryInterface
     */
    private $repository;
    /**
     * @var Hash
     */
    private $hash;

    public function __construct(CmsSlideRepositoryInterface $repository, Hash $hash)
    {

        $this->repository = $repository;
        $this->hash = $hash;
    }

    /**
     * Display a listing of the CmsSlide.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $params = [
            'page' => 1,
            'perPage' => 10,
            'sortField' => 'created_at',
            'sortOrder' => 'desc',
            'searchKeyword' => null,

        ];
        $requestParams = $request->query();
        $params = array_merge($params, $requestParams);
        $query = CmsSlide::orderBy($params['sortField'], $params['sortOrder']);
        if ($params['searchKeyword']) {
            $query = $query->whereLike(['title', 'amount'], $params['searchKeyword']);
        }
        $items = $query->paginate($params['perPage']);
        return successResponse($items);
    }
    public function related()
    {
        $items = $this->repository->related();

        return successResponse($items);
    }

    /**
     * Store a newly created CmsSlide in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CmsSlideStoreRequest $request)
    {
        $data = $request->validated();
        if (isset($data['image'])) {
            $data['image'] = $data['image']->store('uploads', 'public');
        }
        $item = $this->repository->create($data);
        return successResponse($item);
    }

    /**
     * Display the specified CmsSlide.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->repository->get($id);
        return successResponse($item);
    }

    /**
     * Update the specified CmsSlide in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CmsSlideUpdateRequest $request, $id)
    {
        $data = $request->validated();
        // Update Picture in File Storage
        if (isset($data['image'])) {
            $data['image'] = $data['image']->store('uploads', 'public');
        }
        $item = $this->repository->update($id, $data);
        return successResponse($item);
    }

    /**
     * Remove the specified company contact from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->repository->delete($id);
        return successResponse($item);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->ids;
        $this->model->whereIn('id', $ids)->delete();

        return successResponse();
    }
}
