<?php

namespace App\Http\Controllers\CMS;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CMS\SiteLogo;
use App\Resources\CMS\SiteLogoResource;
use App\Resources\CMS\SiteLogoCollection;
use App\Http\Requests\CMS\SiteLogoStoreRequest;
use App\Http\Requests\CMS\SiteLogoUpdateRequest;
use Illuminate\Contracts\Hashing\Hasher as Hash;
use App\Repositories\CMS\SiteLogoRepositoryInterface;

class SiteLogoController extends Controller
{
    /**
     * @var SiteLogoRepositoryInterface
     */
    private $repository;
    /**
     * @var Hash
     */
    private $hash;

    public function __construct(SiteLogoRepositoryInterface $repository, Hash $hash)
    {
        $this->middleware('permission:View Site Logo')->only('index');
        $this->middleware('permission:Create Site Logo')->only('store');
        $this->middleware('permission:Update Site Logo')->only('update');
        $this->middleware('permission:Delete Site Logo')->only('destroy');

        $this->repository = $repository;
        $this->hash = $hash;

    }

    /**
     * Display a listing of the SiteLogo.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $params = [
            'page' => 1,
            'perPage' => 10,
            'sortField' => 'created_at',
            'sortOrder' => 'desc',
            'searchKeyword' => null,

        ];
        $requestParams = $request->query();
        $params = array_merge($params, $requestParams);
        $query = SiteLogo::orderBy($params['sortField'], $params['sortOrder']);
        if ($params['searchKeyword']) {
            $query = $query->whereLike(['header_logo', 'footer_logo', 'favicon'], $params['searchKeyword']);
        }
        $items = $query->paginate($params['perPage']);
        return successResponse(new SiteLogoCollection($items));
    }
    public function related()
    {
        $items = $this->repository->related();
        return ($items);
    }

    /**
     * Store a newly created SiteLogo in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SiteLogoStoreRequest $request)
    {
        $data = $request->validated();
        if (isset($data['header_logo'])) {
            $data['header_logo'] = $data['header_logo']->store('uploads', 'public');
        }
        if (isset($data['footer_logo'])) {
            $data['footer_logo'] = $data['footer_logo']->store('uploads', 'public');
        }
        if (isset($data['favicon'])) {
            $data['favicon'] = $data['favicon']->store('uploads', 'public');
        }
        $item = $this->repository->create($data);
        return ($item);

    }

    /**
     * Display the specified SiteLogo.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->repository->get($id);
        return  successResponse(new SiteLogoResource($item));


    }

    /**
     * Update the specified SiteLogo in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SiteLogoUpdateRequest $request, $id)
    {
        $data = $request->validated();
        if (isset($data['header_logo'])) {
            $data['header_logo'] = $data['header_logo']->store('uploads', 'public');
        }
        if (isset($data['footer_logo'])) {
            $data['footer_logo'] = $data['footer_logo']->store('uploads', 'public');
        }
        if (isset($data['favicon'])) {
            $data['favicon'] = $data['favicon']->store('uploads', 'public');
        }
        $item = $this->repository->update($id, $data);
        return ($item);
    }

    /**
     * Remove the specified company contact from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->repository->delete($id);
        return ($item);
        }
}
