<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CMS\NewsCategory;
use App\Resources\CMS\NewsCategoryResource;
use App\Resources\CMS\NewsCategoryCollection;
use App\Http\Requests\CMS\NewsCategoryStoreRequest;
use App\Http\Requests\CMS\NewsCategoryUpdateRequest;
use Illuminate\Contracts\Hashing\Hasher as Hash;
use App\Repositories\CMS\NewsCategoryRepositoryInterface;

class NewsCategoryController extends Controller
{
    /**
     * @var NewsCategoryRepositoryInterface
     */
    private $repository;
    /**
     * @var Hash
     */
    private $hash;

    public function __construct(NewsCategoryRepositoryInterface $repository, Hash $hash)
    {
        $this->middleware('permission:View News Category')->only('index');
        $this->middleware('permission:Create News Category')->only('store');
        $this->middleware('permission:Update News Category')->only('update');
        $this->middleware('permission:Delete News Category')->only('destroy');
        $this->repository = $repository;
        $this->hash = $hash;
    }

    /**
     * Display a listing of the NewsCategory.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $params = [
            'page' => 1,
            'perPage' => 10,
            'sortField' => 'created_at',
            'sortOrder' => 'desc',
            'searchKeyword' => null,

        ];
        $requestParams = $request->query();
        $params = array_merge($params, $requestParams);
        $query = NewsCategory::orderBy($params['sortField'], $params['sortOrder']);
        if ($params['searchKeyword']) {
            $query = $query->whereLike(['name'], $params['searchKeyword']);
        }
        $items = $query->paginate($params['perPage']);

        return successResponse(new NewsCategoryCollection($items));
    }
    public function related()
    {
        $items = $this->repository->related();

        return ($items);
    }

    /**
     * Store a newly created NewsCategory in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsCategoryStoreRequest $request)
    {
        $data = $request->validated();
        $item = $this->repository->create($data);
        return successResponse($item);
    }

    /**
     * Display the specified NewsCategory.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->repository->get($id);
        return  successResponse(new NewsCategoryResource($item));
    }

    /**
     * Update the specified NewsCategory in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsCategoryUpdateRequest $request, $id)
    {
        $data = $request->validated();
        $item = $this->repository->update($id, $data);
        return ($item);
    }

    /**
     * Remove the specified company contact from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->repository->delete($id);
        return successResponse($item);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->ids;
        $this->model->whereIn('id', $ids)->delete();

        return successResponse();
    }
}
