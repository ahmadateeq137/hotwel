<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CMS\News;
use App\Resources\CMS\NewsResource;
use App\Resources\CMS\NewsCollection;
use App\Http\Requests\CMS\NewsStoreRequest;
use App\Http\Requests\CMS\NewsUpdateRequest;
use Illuminate\Contracts\Hashing\Hasher as Hash;
use App\Repositories\CMS\NewsRepositoryInterface;

class NewsController extends Controller
{
    /**
     * @var NewsRepositoryInterface
     */
    private $repository;
    /**
     * @var Hash
     */
    private $hash;

    public function __construct(NewsRepositoryInterface $repository, Hash $hash)
    {
        $this->middleware('permission:View News')->only('index');
        $this->middleware('permission:Create News')->only('store');
        $this->middleware('permission:Update News')->only('update');
        $this->middleware('permission:Delete News')->only('destroy');
        $this->repository = $repository;
        $this->hash = $hash;
    }

    /**
     * Display a listing of the News.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $params = [
            'page' => 1,
            'perPage' => 10,
            'sortField' => 'created_at',
            'sortOrder' => 'desc',
            'searchKeyword' => null,

        ];
        $requestParams = $request->query();
        $params = array_merge($params, $requestParams);
        $query = News::orderBy($params['sortField'], $params['sortOrder']);
        if ($params['searchKeyword']) {
            $query = $query->whereLike(['news_category_id', 'title',  'status', 'featured'], $params['searchKeyword']);
        }
        $items = $query->paginate($params['perPage']);
        return successResponse(new NewsCollection($items));
    }
    public function related()
    {
        $items = $this->repository->related();

        return successResponse($items);
    }

    /**
     * Store a newly created News in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsStoreRequest $request)
    {
        $data = $request->validated();
        if (isset($data['image'])) {
            $data['image'] = $data['image']->store('uploads', 'public');
        }
        if (isset($data['featured_image'])) {
            $data['featured_image'] = $data['featured_image']->store('uploads', 'public');
        }
        $item = $this->repository->create($data);
        return successResponse($item);
    }

    /**
     * Display the specified News.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->repository->get($id);
        return  successResponse(new NewsResource($item));
    }

    /**
     * Update the specified News in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsUpdateRequest $request, $id)
    {
        $data = $request->validated();
        if (isset($data['image'])) {
            $data['image'] = $data['image']->store('uploads', 'public');
        }
        if (isset($data['featured_image'])) {
            $data['featured_image'] = $data['featured_image']->store('uploads', 'public');
        }
        $item = $this->repository->update($id, $data);
        return successResponse($item);
    }

    /**
     * Remove the specified company contact from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->repository->delete($id);
        return successResponse($item);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->ids;
        $this->model->whereIn('id', $ids)->delete();

        return successResponse();
    }
}
