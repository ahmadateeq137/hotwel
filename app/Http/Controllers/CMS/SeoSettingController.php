<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CMS\SeoSetting;
use App\Resources\CMS\SeoSettingResource;
use App\Http\Requests\CMS\SeoSettingStoreRequest;
use App\Http\Requests\CMS\SeoSettingUpdateRequest;
use Illuminate\Contracts\Hashing\Hasher as Hash;
use App\Repositories\CMS\SeoSettingRepositoryInterface;

class SeoSettingController extends Controller
{
    /**
     * @var SeoSettingRepositoryInterface
     */
    private $repository;
    /**
     * @var Hash
     */
    private $hash;

    public function __construct(SeoSettingRepositoryInterface $repository, Hash $hash)
    {
        $this->middleware('permission:View Seo Setting')->only('index');
        $this->middleware('permission:Create Seo Setting')->only('store');
        $this->middleware('permission:Update Seo Setting')->only('update');
        $this->middleware('permission:Delete Seo Setting')->only('destroy');
        $this->repository = $repository;
        $this->hash = $hash;
    }

    /**
     * Display a listing of the SeoSetting.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $params = [
            'page' => 1,
            'perPage' => 10,
            'sortField' => 'created_at',
            'sortOrder' => 'desc',
            'searchKeyword' => null,

        ];
        $requestParams = $request->query();
        $params = array_merge($params, $requestParams);
        $query = SeoSetting::orderBy($params['sortField'], $params['sortOrder']);
        if ($params['searchKeyword']) {
            $query = $query->whereLike(['meta_title', 'meta_description', 'meta_keywords'], $params['searchKeyword']);
        }
        $items = $query->paginate($params['perPage']);
        return successResponse(SeoSettingResource::Collection($items));
    }
    public function related()
    {
        $items = $this->repository->related();
        return successResponse(SeoSettingResource::Collection($items));
        // return ($items);
    }

    /**
     * Store a newly created SeoSetting in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SeoSettingStoreRequest $request)
    {
        $data = $request->validated();
        if (isset($data['featured_image'])) {
            $data['featured_image'] = $data['featured_image']->store('uploads', 'public');
        }
        $item = $this->repository->create($data);
        return successResponse($item);
    }

    /**
     * Display the specified SeoSetting.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->repository->get($id);
        return  successResponse(new SeoSettingResource($item));
    }

    /**
     * Update the specified SeoSetting in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SeoSettingUpdateRequest $request, $id)
    {
        $data = $request->validated();
        if (isset($data['featured_image'])) {
            $data['featured_image'] = $data['featured_image']->store('uploads', 'public');
        }
        $item = $this->repository->update($id, $data);
        return successResponse($item);
    }

    /**
     * Remove the specified company contact from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->repository->delete($id);
        return successResponse($item);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->ids;
        $this->model->whereIn('id', $ids)->delete();

        return successResponse();
    }
}
