<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CMS\CmsSlider;
use App\Resources\CMS\CmsSliderResource;
use App\Resources\CMS\CmsSliderCollection;
use App\Http\Requests\CMS\CmsSliderStoreRequest;
use App\Http\Requests\CMS\CmsSliderUpdateRequest;
use Illuminate\Contracts\Hashing\Hasher as Hash;
use App\Repositories\CMS\CmsSliderRepositoryInterface;

class CmsSliderController extends Controller
{
    /**
     * @var CmsSliderRepositoryInterface
     */
    private $repository;
    /**
     * @var Hash
     */
    private $hash;

    public function __construct(CmsSliderRepositoryInterface $repository, Hash $hash)
    {

        $this->repository = $repository;
        $this->hash = $hash;
    }

    /**
     * Display a listing of the CmsSlider.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $params = [
            'page' => 1,
            'perPage' => 10,
            'sortField' => 'created_at',
            'sortOrder' => 'desc',
            'searchKeyword' => null,

        ];
        $requestParams = $request->query();
        $params = array_merge($params, $requestParams);
        $query = CmsSlider::orderBy($params['sortField'], $params['sortOrder']);
        if ($params['searchKeyword']) {
            $query = $query->whereLike(['name', 'status'], $params['searchKeyword']);
        }
        $items = $query->paginate($params['perPage']);
        return successResponse($items);
    }
    public function related()
    {
        $items = $this->repository->related();

        return successResponse($items);
    }

    /**
     * Store a newly created CmsSlider in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CmsSliderStoreRequest $request)
    {
        $data = $request->validated();
        if (isset($data['cover_photo'])) {
            $data['cover_photo'] = $data['cover_photo']->store('uploads', 'public');
        }
        $item = $this->repository->create($data);
        return successResponse($item);
    }

    /**
     * Display the specified CmsSlider.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->repository->get($id);
        return successResponse($item);
    }

    /**
     * Update the specified CmsSlider in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CmsSliderUpdateRequest $request, $id)
    {
        $data = $request->validated();
        if (isset($data['cover_photo'])) {
            $data['cover_photo'] = $data['cover_photo']->store('uploads', 'public');
        }
        $item = $this->repository->update($id, $data);
        return successResponse($item);
    }

    /**
     * Remove the specified company contact from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->repository->delete($id);
        return successResponse($item);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->ids;
        $this->model->whereIn('id', $ids)->delete();
        return successResponse();
    }
}
