<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\CMS\Page;
use App\Resources\CMS\PageResource;
use App\Resources\PageCollection;
use App\Http\Requests\CMS\PageStoreRequest;
use App\Http\Requests\CMS\PageUpdateRequest;
use Illuminate\Contracts\Hashing\Hasher as Hash;
use App\Repositories\CMS\PageRepositoryInterface;

class PageController extends Controller
{
    /**
     * @var PageRepositoryInterface
     */
    private $repository;
    /**
     * @var Hash
     */
    private $hash;

    public function __construct(PageRepositoryInterface $repository, Hash $hash)
    {

        // $this->middleware('permission:View Page')->only('index');
        // $this->middleware('permission:Create Page')->only('store');
        // $this->middleware('permission:Update Page')->only('update');
        // $this->middleware('permission:Delete Page')->only('destroy');
        $this->repository = $repository;
        $this->hash = $hash;
    }

    /**
     * Display a listing of the Page.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $params = $request->query();
        $items = $this->repository->paginate($params);
        return successResponse(PageResource::collection($items));
    }
    
    public function related()
    {
        $items = $this->repository->related();

        return successResponse($items);
    }

    /**
     * Store a newly created Page in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageStoreRequest $request)
    {
        $data = $request->validated();
        $data['slug'] = Str::slug($data['title'], '-');
        if (isset($data['featured_image'])) {
            $data['featured_image'] = $data['featured_image']->store('uploads', 'public');
        }
        if (isset($data['description1_image'])) {
            $data['description1_image'] = $data['description1_image']->store('uploads', 'public');
        }
        if (isset($data['description2_image'])) {
            $data['description2_image'] = $data['description2_image']->store('uploads', 'public');
        }
        $item = $this->repository->create($data);

        return successResponse($item);
    }

    /**
     * Display the specified Page.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->repository->get($id);
        return  successResponse(new PageResource($item));
        // return ($item);

    }

    /**
     * Update the specified Page in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PageUpdateRequest $request, $id)
    {
        $data = $request->validated();
        // dd($data);
        $data['slug'] = Str::slug($data['title'], '-');
            if (is_file($data['featured_image'])) {
                $data = $request->validated(['featured_image' => 'mimes:jpeg,jpg,png,gif']);
            $data['featured_image'] = $data['featured_image']->store('uploads', 'public');
            } else {
                unset($data['featured_image']);
            }
        
        
            if (is_file($request['description1_image'])) {
                $data = $request->validated(['description1_image' => 'mimes:jpeg,jpg,png,gif']);
            $data['description1_image'] = $data['description1_image']->store('uploads', 'public');
            } else {
                unset($data['description1_image']);
            }
       

        
            if (is_file($request['description2_image'])) {
                $data = $request->validated(['description2_image' => 'mimes:jpeg,jpg,png,gif']);
            $data['description2_image'] = $data['description2_image']->store('uploads', 'public');
            } else {
                unset($data['description2_image']);
            }
       
        $item = $this->repository->update($id, $data);
        return successResponse($item);
    }


    /**
     * Remove the specified company contact from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->repository->delete($id);
        return successResponse($item);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->ids;
        $this->model->whereIn('id', $ids)->delete();

        return successResponse();
    }
    public function about()
    {
        $item = Page::where('slug', 'about')->first();
        return successResponse($item);
    }

}
