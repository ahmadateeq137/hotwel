<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductSettingStoreRequest;
use App\Http\Requests\ProductSettingUpdateRequest;
use App\Models\Product;
use App\Models\ProductSetting;
use App\Models\Timer;
use App\Repositories\ProductSettingRepositoryInterface;
use Illuminate\Http\Request;

class ProductSettingController extends Controller
{
    /**
     * @var ProductSettingRepositoryInterface
     */
    private $repository;

    public function __construct(ProductSettingRepositoryInterface $repository)
    {
        // $this->middleware('permission:View productSetting')->only('index');
        // $this->middleware('permission:Create productSetting')->only('store');
        // $this->middleware('permission:Update productSetting')->only('update');
        // $this->middleware('permission:Delete productSetting')->only('destroy');
        $this->repository = $repository;
    }

    public function index()
    {
        $productsSetting = ProductSetting::with('products')->get();
        return successResponse($productsSetting);
    }

    public function related()
    {
        $productsSetting = $this->repository->related();
        return successResponse($productsSetting);
    }

    public function store(ProductSettingStoreRequest $request)
    {
        // $product = Product::find($request['product_id']);
        // $data = $request->validated();
        // $item = $this->repository->create($data);
        // $item->products()->attatch($product);

        // return successResponse($item);
    }

    public function show($id)
    {
        $item = $this->repository->get($id);
        return successResponse($item);
    }

    public function update(ProductSettingUpdateRequest $request, $id)
    {
        $data = $request->validated();
        $item = $this->repository->update($id, $data);
        if ($request['status'] == 0) {
            $timer = Timer::where('product_id', $request['product_id'])->get();

            foreach ($timer as $tmr) {

                if ($tmr['status'] == 1) {
                    Timer::where('id', $tmr['id'])->update(['status' => 0]);
                }
            }
        }

        return successResponse($item);
    }

    public function destroy($id)
    {
        $item = $this->repository->delete($id);
        return successResponse($item);
    }
}
