<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Models\Product;
use App\Models\Timer;
use App\Repositories\ProductRepositoryInterface;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->middleware('permission:View product')->only('index');
        $this->middleware('permission:Create product')->only('store');
        // $this->middleware('permission:Update product')->only('update');
        $this->middleware('permission:Delete product')->only('destroy');

        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        if (!$request['user_id']) {
            $data['user_id'] = auth()->id();
        }
        $products = Product::with(['categories', 'timers', 'productSettings'])->where('user_id', $data['user_id'])->get();
        return successResponse($products);
    }

    public function store(ProductStoreRequest $request)
    {
        $data = $request->validated();
        
            if (!$request['user_id']) {
                $data['user_id'] = auth()->id();
            }

        $item = $this->repository->create($data);
        return successResponse($item);
    }

    public function show($macAddress)
    {
        $timer_status = "";
        $product = Product::with(['categories', 'timers', 'productSettings'])->where('mac_address',$macAddress)->first();
       if(isset($product)){
            $timers = $product->timers;
        foreach($timers as $timer){
            if($timer['status'] == 1){
                $mytime = Carbon::now("Asia/Karachi")->format('H:i');
                if(($mytime <= $timer['end_time'] && $mytime >= $timer['start_time'])){
                    $item = $product->productSettings;
                    $item['timer_status'] = $timer['name'];
                    break;
                }else{
                    $timer_status = "Idle";
                    $item = $product->productSettings;
                    $item['timer_status'] = $timer_status;
                }
            }else{
                $item = $product->productSettings;
                $item['timer_status'] = $item['timer_status'];
            }
        }
       }
        
        if(!$product){
            return response([
                'status' => [
                    'success' => false,
                    'code' => 404,
                    'message' => 'Object Not Found',
                ],
            ], 404);
        }

        return successResponse($product);
    }

    public function showWithId($id)
    {
        $timer_status = ""; 
        $product = Product::with(['categories', 'timers',  'productSettings'])->where('id',$id)->first();
        if(isset($product)){
            $timers = $product->timers;
        
        foreach($timers as $timer){
            if($timer['status'] == 1){
                $mytime = Carbon::now("Asia/Karachi")->format('H:i');
                if(($mytime <= $timer['end_time'] && $mytime >= $timer['start_time'])){
                    $item = $product->productSettings;
                    $item['timer_status'] = $timer['name'];
                    break;
                }else{
                    $timer_status = "Idle";
                    $item = $product->productSettings;
                    $item['timer_status'] = $timer_status;
                }
            }else{
                $item = $product->productSettings;
                $item['timer_status'] = $item['timer_status'];
            }
        }
        }
        
        if(!$product){
            return response([
                'status' => [
                    'success' => false,
                    'code' => 404,
                    'message' => 'Object Not Found',
                ],
            ], 404);
        }
        return successResponse($product);
    }

    public function update(ProductUpdateRequest $request, $id)
    {
        $data = $request->validated();

        $item = $this->repository->update($id, $data);
        return successResponse($item);
    }

    public function updateAuto(Request $request, $id)
    {
        $data = $this->validate($request,[
            'auto_shift'=>'boolean',
        ]);

        $item = $this->repository->update($id, $data);
        return successResponse($item);
    }

    public function updateAutoUnAuth(Request $request, $id)
    {
        
        $data = $request->validate([
            'auto_shift'=>'boolean',
        ]);

        $item = $this->repository->update($id, $data);
        return successResponse($item);
    }

    public function updateUnauth(ProductUpdateRequest $request)
    {
        $data = $request->validated();

        $item = $this->repository->update($request['id'], $data);
        return successResponse($item);
    }

    public function destroy($id)
    {
        $product = $this->repository->delete($id);
        return successResponse($product);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->ids;
        $this->model->whereIn('id', $ids)->delete();
        return successResponse();
    }
}
