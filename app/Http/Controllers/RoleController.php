<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleStoreRequest;
use App\Http\Requests\RoleUpdateRequest;
use App\Resources\RoleResource;
use App\Repositories\RoleRepositoryInterface;

/**
 * @group  User Role management
 *
 * APIs for managing user roles
 */
class RoleController extends Controller
{
       /**      
     * @var RoleRepositoryInterface
     */
    private $repository;

    public function __construct(RoleRepositoryInterface $repository)
    {
        $this->middleware('auth:user');
        $this->middleware('permission:View Role')->only('index');
        $this->middleware('permission:Create Role')->only('store');
        $this->middleware('permission:Update Role')->only('update');
        $this->middleware('permission:Delete Role')->only('destroy');
        $this->repository = $repository;
        $this->repository->guard_name = 'user';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = $this->repository->paginate(10);
        // return successResponse($items);
        return successResponse(RoleResource::Collection($items));
    }

    /**
     * Display a listing of the related resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function related()
    {
        $items = $this->repository->related();
        return successResponse($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleStoreRequest $request)
    {
        $data = $request->validated();
        $item = $this->repository->create($data);
        return successResponse($item);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->repository->get($id);
        // return successResponse($item);
        return successResponse(new RoleResource($item));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleUpdateRequest $request, $id)
    {
        $data = $request->validated();
        $item = $this->repository->update($id, $data);
        return successResponse($item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return successResponse();
    }
}
