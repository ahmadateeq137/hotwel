<?php

namespace App\Http\Controllers;

use App\Http\Requests\ForgotRequest;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use App\Repositories\AuthRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class AuthController extends Controller
{
    private $repository;
    private $hash;
    private $model;

    public function __construct(AuthRepositoryInterface $repository, UserRepositoryInterface $user, User $model)
    {
        $this->repository = $repository;
        $this->user = $user;
        $this->model = $model;
        $this->guard = 'user';
        $this->middleware('auth:user')->only(['authenticate', 'logout']);
    }

    /**
     * @param UserStoreRequest $request
     * @return mixed
     */
    public function register(Request $request)
    {

        $data = $request->validate([
            'first_name' => 'required|string|max:100',
            'last_name' => 'required|string|max:100',
            'phone_number' => 'required|min:13|max:13|unique:' . User::class . ',phone_number',
            'password' => 'required|min:6|max:100',
            'verification_token' => 'nullable',
            'email' => 'required|email|unique:' . User::class . ',email',
            'role' => 'sometimes|string|exists:roles,name',
        ]);

        $data['password'] = Hash::make($data['password']);
        $item = $this->user->create($data);
       
        if (isset($data['role'])) {
            $item = $this->user->assignRole($item, $data['role']);
            $permissions = Permission::where('guard_name', 'user')->get();
            $item = $this->user->assignPermission($item, $permissions);
        }

        $login = $this->login($request);
        return $login;
    }

    /**
     * @param LoginRequest $request
     * @return mixed
     */
    public function login(Request $request)
    {
        $credentials = $request->validate([
            'phone_number' => 'required|exists:' . User::class . ',phone_number',
            'password' => 'required|min:8|max:60',
            'remember_me' => 'nullable|boolean'
        ]);

        $us = User::where('phone_number', $credentials['phone_number'])->first();
        if ($us) {
            if ($us->isVerified == 1) {
                $user = $this->repository->attempt($credentials);
                $user['scope'] = $this->getPermissionNames($user);
                $token = $user->createToken('appToken')->accessToken;
                return successResponse([
                    'token' => $token,
                    'user' => $user
                ]);
            }
        } else {
            return abort(404, 'Record not found');
        }
    }

    /**
     * @param ForgotRequest $request
     * @return mixed
     */
    public function forgot(ForgotRequest $request)
    {
        $data = $request->validated();
        return $this->repository->forgot($data);
    }


    /**
     * @param LoginRequest $request
     * @return mixed
     */
    public function resetPassword(LoginRequest $request)
    {
        $data = $request->validated([
            'phone_number' => 'required|phone_number|exists:users',
            'password' => 'required|string|min:6|confirmed'
        ]);
        return $this->repository->resetPassword($data);
    }

    /**
     * Authenticate Employee.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate()
    {
        $user = auth()->user();
        $user['scope'] = $this->getPermissionNames($user);
        return successResponse($user);
    }

    /**
     * Get all Permissions by names assign to a user.
     *
     * @param User $user
     * @return array 
     */
    public function getPermissionNames($user)
    {
        return $user->permissions()->pluck('name')->all();
    }

    public function logout()
    {
        $user = auth()->user()->token();
        $user->revoke();

        return successResponse();
    }

    /**
     * Reset Employee's password.
     *
     * @return \Illuminate\Http\Response
     */
    public function broker()
    {
        return Password::broker('users');
    }
}
