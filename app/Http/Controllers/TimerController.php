<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\TimerStoreRequest;
use App\Http\Requests\TimerUpdateRequest;
use App\Models\Product;
use App\Models\ProductSetting;
use App\Models\Timer;
use App\Repositories\TimerRepositoryInterface;
use App\Resources\TimerResource;
use DateTime;

class TimerController extends Controller
{
    /**
     * @var TimerRepositoryInterface
     */
    private $repository;

    public function __construct(TimerRepositoryInterface $repository)
    {
        $this->middleware('permission:View Timer')->only('index');
        // $this->middleware('permission:Create Timer')->only('store');
        // $this->middleware('permission:Update Timer')->only('update');
        $this->middleware('permission:Delete Timer')->only('destroy');

        $this->repository = $repository;
    }

    /**
     * Display a listing of the Setting.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        // $timers = Timer::with('products')->get();
        // return successResponse($timers);
    }
    public function related()
    {
        $items = $this->repository->related();

        return successResponse($items);
    }

    /**
     * Store a newly created Setting in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(TimerStoreRequest $request)
    // {
    //     $product = Product::find($request['product_id']);
    //     $data = $request->validated();
    //     $item = $this->repository->create($data);
    //     $item->products()->attach($product);   
    //     return successResponse($item); 
    // }

    /**
     * Display the specified Setting.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->repository->get($id);
        return  successResponse(new TimerResource($item));

    }

    /**
     * Update the specified Setting in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TimerUpdateRequest $request, $id)
    {
        $data = $request->validated();
        $tmr = Timer::where('id', $id)->get('product_id')->first();
        $timers = $tmr->where('product_id',$tmr->product_id)->get();
        // return $timers;

        $newFrmtS = DateTime::createFromFormat('H:i', $request['start_time']);
        $newFrmtE = DateTime::createFromFormat('H:i', $request['end_time']);

        $match = '';
        foreach ($timers as  $timer) {


            if ($timer['id'] != $id) {

                $tmrNewFrmtE = DateTime::createFromFormat('H:i', $timer['end_time']);
                $tmrNewFrmtS = DateTime::createFromFormat('H:i', $timer['start_time']);

                if (($newFrmtS <= $tmrNewFrmtE && $newFrmtS >= $tmrNewFrmtS) || ($newFrmtE <= $tmrNewFrmtE && $newFrmtE >= $tmrNewFrmtS)) {

                    $match = '1';
                }
            }
        }
        $item = '';
        if ($match == '1') {
            return response()->json([
                'errors' => "Time duration already exist",
            ], 422);
        } else {
            $item = $this->repository->update($id, $data);
        }

        return successResponse($item);
    }

    public function updateUnauth(TimerUpdateRequest $request, $mac_address)
    {

        $name = $request['name'];
        
        $data = $request->validated();
        $timers = Product::where('mac_address', $mac_address)->with(['timers', 'productSettings'])->first();

        if(isset($timers)){

            if($name == 'settings' || $name == ''){
                $data = $request->validate([
                    'gass'=>'nullable|boolean',
                    'electricity'=>'nullable|boolean',
                    'gas_status'=>'nullable|boolean',
                    'electricity_status'=>'nullable|boolean',
                    'temperature_status'=>'nullable',
                    'auto'=>'nullable|boolean',
                    'temperature'=>'nullable',
                    'status'=>'nullable|boolean',
                ]);

                $timer = $timers->productSettings;
                $item = ProductSetting::where('id', $timer['id'])->first();
                $item->update($data);
                return successResponse($item);
            }else{
                $timers = $timers->timers;
                $tmr = $timers->where('name',$name)->first();
                $newFrmtS = DateTime::createFromFormat('H:i', $request['start_time']);
                $newFrmtE = DateTime::createFromFormat('H:i', $request['end_time']);

                $match = '';
                foreach ($timers as  $timer) {


                    if ($timer['id'] != $tmr['id']) {

                        $tmrNewFrmtE = DateTime::createFromFormat('H:i', $timer['end_time']);
                        $tmrNewFrmtS = DateTime::createFromFormat('H:i', $timer['start_time']);

                        if (($newFrmtS <= $tmrNewFrmtE && $newFrmtS >= $tmrNewFrmtS) || ($newFrmtE <= $tmrNewFrmtE && $newFrmtE >= $tmrNewFrmtS)) {

                            $match = '1';
                        }
                    }
                }
                $item = '';
                if ($match == '1') {
                    return response()->json([
                        'errors' => "Time duration already exist",
                    ], 422);
                } else {
                    $item = $this->repository->update($tmr['id'], $data);
                }

            }
            return successResponse($item);
        }
    }

    /**
     * Remove the specified company contact from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = $this->repository->delete($id);
        return successResponse($item);
    }
}
