<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;
use App\Image\Compress;
use App\Models\Category;
use App\Repositories\CategoryRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    private $repository;

    public function __construct(CategoryRepositoryInterface $repository)
    {
        $this->middleware('permission:View Category')->only('index');
        // $this->middleware('permission:Create Category')->only('store');
        $this->middleware('permission:Update Category')->only('update');
        $this->middleware('permission:Delete Category')->only('destroy');

        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        if (!$request['user_id']) {
            $data['user_id'] = auth()->id();
        }
        $uid = $data['user_id'];
        $Category = Category::with(['products' => function ($q) use($uid){
            $q->where('user_id', $uid);
        }])->get();
        return successResponse($Category);
    }

    // public function store(CategoryStoreRequest $request)
    // {
    //     $data = $request->validated();
    //     $user = auth()->user();

    //     if (!$request['user_id']) {
    //         $data['user_id'] = auth()->id();
    //     }
    //     $item = $this->repository->create($data);
    //     return successResponse($item);
    // }

    public function show(Request $request, $id)
    {
        if (!$request['user_id']) {
            $data['user_id'] = auth()->id();
        }
        $uid = $data['user_id'];
        $Category = Category::with(['products' => function ($q) use($uid){
            $q->where('user_id', $uid);
        }])->where('id', $id)->first();
        
        if(!$Category){
            return response([
                'status' => [
                    'success' => false,
                    'code' => 404,
                    'message' => 'Object Not Found',
                ],
            ], 404);
        }
        return successResponse($Category);
    }

    public function update(CategoryUpdateRequest $request, $id)
    {
        $data = $request->validated();
        
        if (isset($data['image']) && getType($data['image']) != 'string') {
            $request->validated([
                'image'=>'nullable|mimes:jpg,jpeg,png'
            ]);
            $image = $request->file('image');
            $compress = new Compress();
            $data['image'] = $compress->resizeImage($image);
            Storage::disk('local')->put($data['image'], 'public');
        }
        $item = $this->repository->update($id, $data);
        return successResponse($item);
    }

    // public function destroy($id)
    // {
    //     $Category = $this->repository->delete($id);
    //     return successResponse($Category);
    // }

    // public function deleteMultiple(Request $request)
    // {
    //     $ids = $request->ids;
    //     $this->model->whereIn('id', $ids)->delete();
    //     return successResponse();
    // }
}
