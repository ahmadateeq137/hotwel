<?php

namespace App\Http\Controllers;

use App\Image\Compress;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Product;
use App\Models\User;
use App\Models\Role;
use App\Repositories\UserRepositoryInterface;
use App\Resources\UserResource;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $repository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->middleware('permission:View User')->only('index');
        $this->middleware('permission:Update User')->only('update');
        $this->middleware('permission:Delete User')->only('destroy');
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $item = User::where('id', auth()->id())->with('products')->withCount(['products As geyser_count' => function ($q) {
            $q->where('category_id', 1);
        }])->withCount(['products As boiler_count' => function ($q) {
            $q->where('category_id', 2);
        }])->orderBy('id', 'desc')->paginate(10);
        return successResponse($item);
    }

    public function related()
    {
        $items = $this->repository->related();
        return successResponse($items);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->repository->get($id);
        return  successResponse(new UserResource($item));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $user = User::where('id', $id)->first();
        if (!$request['first_name']) {
            $request['first_name'] = $user->first_name;
        }
        if (!$request['last_name']) {
            $request['last_name'] = $user->last_name;
        }
        $data = $request->validated();
        
        if (isset($data['image']) && getType($data['image']) != 'string') {
            $request->validated([
                'image'=>'nullable|mimes:jpg,jpeg,png'
            ]);
            $image = $request->file('image');
            $compress = new Compress();
            $data['image'] = $compress->resizeImage($image);
        }
        $item = $this->repository->update($id, $data);
        
        if (isset($data['role'])) {
            $item = $this->repository->syncRole($item, $data['role']);
            $role = Role::where('name', $data['role'])->with('permissions')->first();
            $permissions = $role->permissions;
            $item = $this->repository->syncPermission($item, $permissions);
        }

        return successResponse($item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return successResponse();
    }
}
