<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\CRUD\Traits\PaginateTrait;
use App\Resources\ContactResource;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactStoreRequest;
use App\Http\Requests\ContactUpdateRequest;
use App\Repositories\ContactRepositoryInterface;


class ContactController extends Controller
{
    use PaginateTrait;

    private $repository;

    public function __construct(ContactRepositoryInterface $repository)
    {
        $this->middleware('permission:View Contact')->only('index');
        $this->middleware('permission:Create Contact')->only('store');
        $this->middleware('permission:Update Contact')->only('update');
        $this->middleware('permission:Delete Contact')->only('destroy');
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $Contactes = Contact::where('user_id', auth()->id())->with('user')->orderBy('id', 'desc')->paginate(10);
        return successResponse(ContactResource::collection($Contactes));
    }

    public function related()
    {
        $items = $this->repository->related();
        return successResponse($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactStoreRequest $request)
    {
        $data = $request->validated();
        $user = auth()->user();

        if (!$request['user_id']) {
            $data['user_id'] = auth()->id();
        }
        if (!$request['first_name']) {
            $data['first_name'] = $user->first_name;
        }
        if (!$request['last_name']) {
            $data['last_name'] = $user->last_name;
        }
        if (!$request['phone']) {
            $data['phone'] = $user->phone_number;
        }
        if (!$request['email']) {
            $data['email'] = $user->email;
        }
        $item = $this->repository->create($data);

        return successResponse($item);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = $this->repository->get($id);
        return  successResponse(new ContactResource($item));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContactUpdateRequest $request, $id)
    {
        $data = $request->validated();

        if (!$request->user_id) {
            $request->user_id = auth()->id();
        }
        $item = $this->repository->update($id, $data);
        return successResponse($item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return successResponse();
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->ids;
        $this->model->whereIn('id', $ids)->delete();

        return successResponse();
    }
}
