<?php

use App\Http\Controllers\GeyserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [\App\Http\Controllers\AuthController::class, 'login']);
Route::post('register', [\App\Http\Controllers\AuthController::class, 'register']);
Route::post('verify', [\App\Http\Controllers\AuthController::class, 'verify']);
Route::post('resendotp', [\App\Http\Controllers\AuthController::class, 'resendOTP']);
Route::post('forgot', [\App\Http\Controllers\AuthController::class, 'forgot']);
Route::post('opt_verification', [\App\Http\Controllers\AuthController::class, 'verification']);
Route::post('resetPassword', [\App\Http\Controllers\AuthController::class, 'resetPassword']);

Route::prefix('Unauth')->group(function (){
    Route::get('product/show/{mac_address}', '\App\Http\Controllers\ProductController@show');
    Route::get('product/showWithId/{id}', '\App\Http\Controllers\ProductController@showWithId');
    Route::put('product/update', '\App\Http\Controllers\ProductController@updateUnauth');
    Route::get('timer/show/{id}', '\App\Http\Controllers\TimerController@show');
    Route::put('timer/update/{mac_address}', '\App\Http\Controllers\TimerController@updateUnauth');
    Route::put('updateAuto/{id}', '\App\Http\Controllers\ProductController@updateAutoUnAuth');
});

Route::middleware('auth:user')->group(function () {

    Route::namespace('\App\Http\Controllers')->group(function () {
        Route::post('logout', 'AuthController@logout');
        Route::get('authenticate', 'AuthController@authenticate');

        Route::get('contact/related', 'ContactController@related');
        Route::apiResource('contact', 'ContactController');

        Route::get('user/related', 'UserController@related');
        Route::apiResource('user', 'UserController');

        Route::apiResource('category', 'CategoryController');
        Route::apiResource('product', 'ProductController');
        Route::put('product/updateAuto/{id}', 'ProductController@updateAuto');
        Route::get('product/showWithId/{id}', 'ProductController@showWithId');
        
        Route::apiResource('setting', 'SettingController');

        Route::apiResource('commonsetting', 'CommonSettingController');

        Route::get('timer/related', 'TimerController@related');
        Route::apiResource('timer', 'TimerController');
        
        Route::get('productSetting/related','ProductSettingController@related');
        Route::apiResource('productSetting','ProductSettingController');

    });

    Route::namespace('\App\Http\Controllers\CMS')->group(function () {
        Route::get('about', 'PageController@about');
        Route::put('about/{id}', 'PageController@update');

        Route::get('seosetting/related', 'SeoSettingController@related');
        Route::apiResource('seosetting', 'SeoSettingController');
        Route::delete('seosetting/deleteMultiple', 'SeoSettingController@deleteMultiple');

        Route::get('news/related', 'NewsController@related');
        Route::apiResource('news', 'NewsController');
        Route::delete('news/deleteMultiple', 'NewsController@deleteMultiple');

        Route::apiResource('newsletter', 'NewsletterController');
        Route::delete('newsletter/deleteMultiple', 'NewsletterController@deleteMultiple');

        Route::get('newscategory/related', 'NewsCategoryController@related');
        Route::apiResource('newscategory', 'NewsCategoryController');
        Route::delete('newscategory/deleteMultiple', 'NewsCategoryController@deleteMultiple');

        Route::get('faq/related', 'FaqController@related');
        Route::apiResource('faq', 'FaqController');
        Route::delete('faq/deleteMultiple', 'FaqController@deleteMultiple');

        Route::get('page/related', 'PageController@related');
        Route::apiResource('page', 'PageController');
        Route::delete('page/deleteMultiple', 'PageController@deleteMultiple');

        Route::apiResource('sitelogo', 'SiteLogoController');

        Route::get('cmsslider/related', 'CmsSliderController@related');
        Route::apiResource('cmsslider', 'CmsSliderController');

        Route::get('cmsslide/related', 'CmsSlideController@related');
        Route::apiResource('cmsslide', 'CmsSlideController');
    });
});
